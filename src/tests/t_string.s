;;----------------------------------------------------------------------------------------------------------------------
; string.s tests
; coverage: isDigit, isHexDigit, isIdentChar, isAlpha, isUpperAlpha, upperCase, strSkipWS,
;       strlen, strEnd, strcpy, strSliceCopy, strcmp
; missing: strFindWS (not used in Odin currently)

__test_string_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"String: "
                ; shared module must be already paged in (otherwise colouredPrint used above did crash)

                ; TEST isDigit
                __T_SETUP 'D'
                ld hl,0 : ld de,hl      ; HL = DE = 0
                ld      a,'0'
.isDigitL1:     call    isDigit
                adc     hl,de
                inc     a
                cp      '0'+10
                jr      nz,.isDigitL1   ; going through all digit chars ('0'..'9')
                ex      de,hl   ; here DE and HL should be still zero (isDigit should have returned CF=0)
                xor     a
.isDigitL2:     call    isDigit ; so if there was something unexpected in HL, it will ruin second loop from DE
                adc     hl,de
                inc     a
                jr      nz,.isDigitL2   ; going through all 256 values (should result in HL == 256-10 result)
                __T_CHECK only_de_hl : dw 0, 256-10

                ; TEST isHexDigit
                __T_SETUP 'H'
                ld hl,0 : ld de,hl      ; HL = DE = 0
                ld      a,'0'
.isHexDigitL1:
                cp '9'+1 : jr nz,.isHexDigitN1 : ld a,'A'  ; jump after '9' to 'A'
.isHexDigitN1:  cp 'F'+1 : jr nz,.isHexDigitN2 : ld a,'a'  ; jump after 'F' to 'a'
.isHexDigitN2:  call    isHexDigit      ; check '0-9', 'A-F', 'a-f'
                adc     hl,de
                inc     a
                cp      'f'+1
                jr      nz,.isHexDigitL1
                ex      de,hl   ; here DE and HL should be still zero (isHexDigit should have returned CF=0)
                xor     a       ; non-zero in HL will ruin following loop from DE
.isHexDigitL2:  call    isHexDigit      ; loops through all 256 chars (including hex-digits)
                adc     hl,de
                inc     a
                jr      nz,.isHexDigitL2
                __T_CHECK only_de_hl : dw 0, 256-16-6

                ; TEST isIdentChar
                __T_SETUP 'I'
                ld hl,0 : ld de,hl      ; HL = DE = 0
                ld      a,'_'
                call    isIdentChar     ; check '_' separately out of loop
                adc     hl,de
                ld      a,'.'
                call    isIdentChar
                adc     hl,de
                ld      a,'0'
.isIdCharL1:
                cp '9'+1 : jr nz,.isIdCharN1 : ld a,'A'  ; jump after '9' to 'A'
.isIdCharN1:    cp 'Z'+1 : jr nz,.isIdCharN2 : ld a,'a'  ; jump after 'Z' to 'a'
.isIdCharN2:    call    isIdentChar     ; check '0-9', 'A-Z', 'a-z'
                adc     hl,de
                inc     a
                cp      'z'+1
                jr      nz,.isIdCharL1
                ex      de,hl   ; here DE and HL should be still zero (isIdentChar should have returned CF=0)
                xor     a       ; non-zero in HL will ruin following loop from DE
.isIdCharL2:    call    isIdentChar     ; loops through all 256 chars (including identifier chars)
                adc     hl,de
                inc     a
                jr      nz,.isIdCharL2
                __T_CHECK only_de_hl : dw 0, 256-2-10-26-26

                ; TEST isAlpha
                __T_SETUP 'a'
                ld hl,0 : ld de,hl      ; HL = DE = 0
                ld      a,'A'
.isAlphaL1:
                cp 'Z'+1 : jr nz,.isAlphaN1 : ld a,'a'  ; jump after 'Z' to 'a'
.isAlphaN1:     call    isAlpha         ; check 'A-Z', 'a-z'
                adc     hl,de
                inc     a
                cp      'z'+1
                jr      nz,.isAlphaL1
                ex      de,hl   ; here DE and HL should be still zero (isAlpha should have returned CF=0)
                xor     a       ; non-zero in HL will ruin following loop from DE
.isAlphaL2:     call    isAlpha         ; loops through all 256 chars (including identifier chars)
                adc     hl,de
                inc     a
                jr      nz,.isAlphaL2
                __T_CHECK only_de_hl : dw 0, 256-26-26

                ; TEST isUpperAlpha
                __T_SETUP 'A'
                ld hl,0 : ld de,hl      ; HL = DE = 0
                ld      a,'A'
.isUAlphaL1:    call    isUpperAlpha    ; check 'A-Z'
                adc     hl,de
                inc     a
                cp      'Z'+1
                jr      nz,.isUAlphaL1
                ex      de,hl   ; here DE and HL should be still zero (isAlpha should have returned CF=0)
                xor     a       ; non-zero in HL will ruin following loop from DE
.isUAlphaL2:    call    isUpperAlpha    ; loops through all 256 chars (including identifier chars)
                adc     hl,de
                inc     a
                jr      nz,.isUAlphaL2
                __T_CHECK only_de_hl : dw 0, 256-26

                ; TEST upperCase
                __T_SETUP 'U'
                ld      de,'aA'         ; D = test input, E = expected result
.UpperCaseL1:   ld      a,d             ; first loop going through 'a-z', checking result for A-Z
                call    upperCase
                cp      e
                call    __t_cmp_zf
                inc e : inc d           ; next chars in d/e
                cp      'Z'
                jr      nz,.UpperCaseL1
.UpperCaseL2:   ld      a,d             ; second loop going through everything else (not 'a-z')
                call    upperCase
                cp      d               ; shouldn't change
                call    __t_cmp_zf
                inc     d               ; next char
                cp      'a'-1
                jr      nz,.UpperCaseL2
                __T_CHECK none

                ; TEST strSkipWS
                __T_SETUP 'w'
                ld      hl,__t_string_s1
                call    strSkipWS
                __T_CHECK a_hl : db 'a' : dw __t_string_s1
                __T_SETUP '2'
                ld      hl,__t_string_s1+1  ; empty string (just 0)
                call    strSkipWS
                __T_CHECK a_hl : db 0 : dw __t_string_s1+1
                __T_SETUP '3'
                ld      hl,__t_string_sW    ; empty string with leading space
                call    strSkipWS
                __T_CHECK a_hl : db 0 : dw __t_string_sW+1
                __T_SETUP '4'
                ld      hl,__t_string_s1W
                call    strSkipWS
                __T_CHECK a_hl : db 'a' : dw __t_string_s1

                ;TODO strFindWS - currently untested because not used by odin (so to not lock API with test)

                ; TEST strDec
                __T_SETUP 'N'
                ld      hl,__t_string_s1W
                call    strDec      ; " a"
                call    __t_cmp_cf
                ld      hl,__t_string_s1
                call    strDec      ; "a"
                call    __t_cmp_cf
                ld      hl,__t_string_s1+1
                call    strDec      ; ""
                __T_CHECK only_cf
                __T_SETUP '2'
                ld      hl,__t_string_n1
                call    strDec
                __T_CHECK only_ncf_de_hl_bc : dw __t_string_n1.v>>>16, __t_string_n1.v&$FFFF, __t_string_n1.e
                __T_SETUP '3'
                ld      hl,__t_string_n2
                call    strDec
                __T_CHECK only_ncf_de_hl_bc : dw __t_string_n2.v>>>16, __t_string_n2.v&$FFFF, __t_string_n2.e
                __T_SETUP '4'
                ld      hl,__t_string_n3
                call    strDec
                __T_CHECK only_ncf_de_hl_bc : dw __t_string_n3.v>>>16, __t_string_n3.v&$FFFF, __t_string_n3.e

                ; TEST strcmp
                __T_SETUP 'c'
                ld      de,__t_string_s1
                ld      hl,__t_string_s1
                call    strcmp
                __T_CHECK zf_a_hl_de : db 0 : dw __t_string_s1, __t_string_s1
                __T_SETUP '2'
                ld      de,__t_string_s1
                ld      hl,__t_string_s2
                call    strcmp
                __T_CHECK nzf_a_hl_de : db -'b' : dw __t_string_s2, __t_string_s1
                __T_SETUP '3'
                ld      de,__t_string_s2
                ld      hl,__t_string_s3
                call    strcmp
                __T_CHECK nzf_a_hl_de : db 'd'-'D' : dw __t_string_s3, __t_string_s2

                ; TEST strlen
                __T_SETUP 'l'
                ld      hl,__t_string_s1
                call    strlen
                __T_CHECK flags_hl_bc : dw __t_string_s1, __t_string_s1.l
                __T_SETUP '2'
                ld      hl,__t_string_s2
                call    strlen
                __T_CHECK flags_hl_bc : dw __t_string_s2, __t_string_s2.l

                ; TEST strEnd
                __T_SETUP 'e'
                ld      hl,__t_string_s1
                call    strEnd
                __T_CHECK flags_hl : dw __t_string_s1+__t_string_s1.l
                __T_SETUP '2'
                ld      hl,__t_string_s2
                call    strEnd
                __T_CHECK flags_hl : dw __t_string_s2+__t_string_s2.l

                ; TEST strcpy
                __T_SETUP 'C'
                    ld      hl,__t_string_s1
                    ld      de,__t_string_buf
                    call    strcpy
                    __T_WORDCMP __t_string_buf, 'a' ; upper byte is 0
                    __T_CHECK zf_a_hl_de : db 0 : dw __t_string_s1+__t_string_s1.l+1, __t_string_buf+__t_string_s1.l+1
                __T_SETUP '2'
                    ld      hl,__t_string_s2
                    ld      de,__t_string_buf
                    call    strcpy      ; first the resulting regs are tested, then `strcmp` is re-used for actual result in memory
                    __T_CHECK zf_a_hl_de : db 0 : dw __t_string_s2+__t_string_s2.l+1, __t_string_buf+__t_string_s2.l+1
                    __T_SETUP '.'
                    ld      de,__t_string_s2
                    ld      hl,__t_string_buf
                    call    strcmp
                    __T_CHECK zf_a_hl_de : db 0 : dw __t_string_buf, __t_string_s2

                ; TEST strSliceCopy
                __T_SETUP 'S'
                    ld      hl,__t_string_s2    ; copy "abc" out of s2 to second area of buf (+8)
                    ld      bc,__t_string_s2+3
                    ld      de,__t_string_buf+8
                    call    strSliceCopy
                    ld      hl,__t_default_hl   ; ignore HL change
                    __T_MEMCMP __t_string_buf+8, "abc\0"
                    __T_CHECK flags_a_de_bc : db 'd' : dw __t_string_buf+12, __t_string_s2+3
                    __T_SETUP '.'
                    ld      hl,__t_string_s3    ; copy "abc" out of s3 (!)
                    ld      bc,__t_string_s3+3
                    ld      de,__t_string_buf
                    call    strSliceCopy
                    ld      hl,__t_default_hl   ; ignore HL change
                    __T_MEMCMP __t_string_buf, "abc\0"
                    __T_CHECK flags_a_de_bc : db 'D' : dw __t_string_buf+4, __t_string_s3+3
                __T_SETUP '2'
                    ld      hl,__t_string_s2    ; copy "" out of s2 after the "a" which is currently in buf at first char
                    ld      bc,__t_string_s2
                    ld      de,__t_string_buf+1
                    call    strSliceCopy
                    ld      hl,__t_default_hl   ; ignore HL change
                    __T_WORDCMP __t_string_buf, 'a' ; upper byte is 0
                    __T_CHECK flags_a_de_bc : db 'a' : dw __t_string_buf+2, __t_string_s2

                ; test any extra overwrite in the buffer (there should be canary string after it)
                __T_SETUP '!'
                __T_MEMCMP __t_string_bcan, "$@$\0"
                __T_CHECK none

                ret

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_string_sW   dz      " "
__t_string_s1W  db      ' '             ; leading space for "s1" string
__t_string_s1   dz      "a"
.l              equ     $-__t_string_s1-1
__t_string_s2   dz      "abcdefghijklmnopqrstuvwxyz"
.l              equ     $-__t_string_s2-1
__t_string_s3   dz      "abcD"
__t_string_buf  db      "___________________________"
__t_string_bcan dz      "$@$"
__t_string_n1   dz      "7"             ; string, .e = end ptr (at "\0"), .v = value
.e              equ     $-1
.v              equ     7
__t_string_n2   dz      "  259"
.e              equ     $-1
.v              equ     259
__t_string_n3   dz      "4294967295"
.e              equ     $-1
.v              equ     4294967295

                display "[TEST RUN] tests: string.s                     Space: ",/A,$-__test_string_s
