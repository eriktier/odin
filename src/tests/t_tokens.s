;;----------------------------------------------------------------------------------------------------------------------
; tokens.s tests
; coverage: tokenise, tokeniseLine, detokenise, findToken

__test_tokens_s:
                ld a,7 : call colouredPrint : dz C_ENTER,"Tokens: "
                ; shared module must be already paged in (otherwise colouredPrint used above did crash)

                ; TEST findToken
                __T_SETUP 'f'
                ld      hl,__t_tokens_tokentab
                ld      bc,$8000            ; token to find "ADC"
                call    findToken
                ld      a,__t_default_a     ; ignore changes in A
                __T_CHECK ncf_hl_bc : dw __t_tokens_tokentab.adc+2, $8000
                __T_SETUP '2'
                ld      hl,__t_tokens_tokentab
                ld      bc,$8e02            ; token to find "BYTE" (second DB alias)
                call    findToken
                ld      a,__t_default_a     ; ignore changes in A
                __T_CHECK ncf_hl_bc : dw __t_tokens_tokentab.byte+2, $8e02
                __T_SETUP '3'
                ld      hl,__t_tokens_tokentab
                ld      bc,$8e01            ; token to find "EDFB" (first DB alias)
                call    findToken
                ld      a,__t_default_a     ; ignore changes in A
                __T_CHECK ncf_hl_bc : dw __t_tokens_tokentab.defb+2, $8e01
                __T_SETUP '4'
                ld      hl,__t_tokens_tokentab
                ld      bc,$8e00            ; token to find "DB"
                call    findToken
                ld      a,__t_default_a     ; ignore changes in A
                __T_CHECK ncf_hl_bc : dw __t_tokens_tokentab.db+2, $8e00
                __T_SETUP '5'
                ld      hl,__t_tokens_tokentab
                ld      bc,$fe00            ; unknown token to find (but not OP_UNKNOWN=$ff ! that's table terminator)
                call    findToken
                ld      a,__t_default_a     ; ignore changes in A
                __T_CHECK cf_hl_bc : dw __t_tokens_tokentab.end+1, $fe00

                ; TEST tokenise
                __T_SETUP 't'
                ld      de,__t_tokens_txt_input1
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry      ; skip regular entry to have custom token table in HL
                __T_CHECK only_cf_de : dw __t_tokens_txt_input1
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input1
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input1
                __T_SETUP '2'
                ld      de,__t_tokens_txt_input1+2  ; just null terminator
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input1+2
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input1+2  ; just null terminator
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input1+2
                __T_SETUP '3'
                ld      de,__t_tokens_txt_input3    ; "DBCCF"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input3
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input3    ; "DBCCF"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input3
                __T_SETUP '4'
                ld      de,__t_tokens_txt_input2    ; "CCF\0"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_input2+3, __t_tokens_txt_input2, $8500
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input2    ; "CCF\0"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_input2+3, __t_tokens_txt_input2, $8500
                __T_SETUP '5'
                ld      de,__t_tokens_txt_input4    ; "CCF A"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_input4+4, __t_tokens_txt_input4, $8500
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input4    ; "CCF A"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_input4+3, __t_tokens_txt_input4, $8500
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input4b   ; "CCF ;"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_input4b+3, __t_tokens_txt_input4b, $8500
                __T_SETUP '6'
                ld      de,__t_tokens_txt_input5    ; "CCF_"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input5
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input5    ; "CCF_"
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input5
                __T_SETUP '7'
                ld      de,__t_tokens_txt_input6    ; "CCF<" (fail as instruction)
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_input6
                __T_SETUP '.'
                ld      de,__t_tokens_txt_input6    ; "CCF<" (find as operand)
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_input6+3, __t_tokens_txt_input6, $8500
                ; try all keywords from the table
                __T_SETUP '8'
                ld      de,__t_tokens_txt_inp_adc
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_adc+3, __t_tokens_txt_inp_adc, $8000
                __T_SETUP '.'
                ld      de,__t_tokens_txt_inp_adc
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_adc+3, __t_tokens_txt_inp_adc, $8000
                __T_SETUP ':'
                ld      de,__t_tokens_txt_inp_add
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_add+3, __t_tokens_txt_inp_add, $8100
                __T_SETUP '.'
                ld      de,__t_tokens_txt_inp_add
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_add+3, __t_tokens_txt_inp_add, $8100
                __T_SETUP ':'
                ld      de,__t_tokens_txt_inp_byt
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_byt+4, __t_tokens_txt_inp_byt, $8e02
                __T_SETUP '.'
                ld      de,__t_tokens_txt_inp_byt
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_byt+4, __t_tokens_txt_inp_byt, $8e02
                __T_SETUP ':'
                ld      de,__t_tokens_txt_inp_db
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_db+2, __t_tokens_txt_inp_db, $8e00
                __T_SETUP '.'
                ld      de,__t_tokens_txt_inp_deb
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_deb+4, __t_tokens_txt_inp_deb, $8e01
                __T_SETUP '.'
                ld      de,__t_tokens_txt_inp_xor
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_xor+3, __t_tokens_txt_inp_xor, $ca00
                __T_SETUP '9'
                ld      de,__t_tokens_txt_inp_db2
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_ins
                call    tokenise.entry
                __T_CHECK only_cf_de : dw __t_tokens_txt_inp_db2
                __T_SETUP '.'
                ld      de,__t_tokens_txt_inp_db2
                ld      hl,__t_tokens_tokentab
                ld      ix,tokenise_is_op
                call    tokenise.entry
                __T_CHECK only_ncf_de_hl_bc : dw __t_tokens_txt_inp_db2+2, __t_tokens_txt_inp_db2, $8e00

                ; TEST tokeniseLine
                __T_SETUP 'T'
                ld      de,__t_tokens_txt_line1
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_BYTECMP __t_tokens_buf_out, 0
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line1, __t_tokens_buf_out, $0000
                __T_SETUP '2'
                ld      de,__t_tokens_txt_line2
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, "ccfo\0"
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line2+4, __t_tokens_buf_out+4, $0100
                __T_SETUP '3'
                ld      de,__t_tokens_txt_line3
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_WORDCMP __t_tokens_buf_out, OP_CCF  ; upper byte is 0
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line3+3, __t_tokens_buf_out+1, $0000
                __T_SETUP '4'
                ld      de,__t_tokens_txt_line4
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <';', $0A, 24, "ccf\0">
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line4+29, __t_tokens_buf_out+6, $0201
                __T_SETUP '5'
                ld      de,__t_tokens_txt_line5
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <OP_LDRX, 1, OP_A, 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line5+7, __t_tokens_buf_out+3, $0000
                ; test different amount of spaces compressing to [$0a,count] or $0b..$20
                __T_SETUP '6'
                ld      de,__t_tokens_txt_line6
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <$0A, 40, OP_NOP, 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line6+43, __t_tokens_buf_out+3, $0000
                __T_SETUP '.'
                ld      de,__t_tokens_txt_line6+17
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <$0A, 23, OP_NOP, 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line6+43, __t_tokens_buf_out+3, $0000
                __T_SETUP ':'
                ld      de,__t_tokens_txt_line6+18
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <$0B, OP_NOP, 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line6+43, __t_tokens_buf_out+2, $0000
                __T_SETUP '.'
                ld      de,__t_tokens_txt_line6+39
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <' ', OP_NOP, 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line6+43, __t_tokens_buf_out+2, $0000
                ; testing case which failed in dev.2c
                __T_SETUP '7'
                ld      de,__t_tokens_txt_line7     ; working, not derailed by ';'
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <OP_LD, OP_A, ",+($b", OP_SHL, "1)+\";\"\0">
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line7+17, __t_tokens_buf_out+14, $0000
                __T_SETUP '.'
                ld      de,__t_tokens_txt_line7b    ; was derailed by ';', missing SHL
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <OP_LD, OP_A, ",+\";\"+($b", OP_SHL, "1)\0">
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line7b+17, __t_tokens_buf_out+14, $0000
                ; regular complex case including label + instruction + comment at one line
                __T_SETUP '8'
                ld      de,__t_tokens_txt_line8
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <"l1 ",OP_BSRL, OP_DE, ',', OP_B, " ; de,b\0">
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line8+19, __t_tokens_buf_out+14, $0200
                ; check compression of spaces inside quotes
                __T_SETUP '9'
                ld      de,__t_tokens_txt_line9
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <OP_DEFB, '"', $1E, '"', 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line9+8, __t_tokens_buf_out+4, $0000
                ; the `ret;comment` case from discussion with Matt, should [de]tokenise!
                __T_SETUP '0'
                ld      de,__t_tokens_txt_line10
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <OP_RET, ';', 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line10+4, __t_tokens_buf_out+2, $0200
                __T_SETUP '.'
                ld      de,__t_tokens_txt_line10b
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <OP_RET, " ;", 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line10b+5, __t_tokens_buf_out+3, $0200
                ; test the new feature - reporting missing closing quotes
                __T_SETUP '1'
                ld      de,__t_tokens_txt_line11
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <$1F, OP_LD, OP_A, ',"', 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line11+8, __t_tokens_buf_out+5, '"'<<8
                __T_SETUP '.'
                ld      de,__t_tokens_txt_line11b
                ld      hl,__t_tokens_buf_out
                call    tokeniseLine
                __T_MEMCMP __t_tokens_buf_out, <$1F, OP_LD, OP_B, ",'x", 0>
                __T_CHECK only_de_hl_bc : dw __t_tokens_txt_line11b+9, __t_tokens_buf_out+6, "'"<<8

                ; check canary at end of output buffer
                __T_SETUP '!'
                __T_MEMCMP __t_tokens_buf_out.can, "$@$\0"
                __T_CHECK none

                ; create canary marker "$@$\0" after the __t_tokens_vram_buf buffer
                ld      hl,'@$'
                ld      (__t_tokens_vram_buf+$100),hl
                ld      hl,'$'
                ld      (__t_tokens_vram_buf+$102),hl

                ; TEST detokenise
                __T_SETUP 'D'
                ld      hl,__t_tokens_tok_line1
                ld      de,__t_tokens_vram_buf
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf, "CCF\0"
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+3, __t_tokens_tok_line1+1
                __T_SETUP '2'
                ld      hl,__t_tokens_tok_line2
                ld      de,__t_tokens_vram_buf
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf, "l1  BSRL DE,  B ; de,b\0"
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+22, __t_tokens_tok_line2+17
                __T_SETUP '3'
                ld      hl,__t_tokens_tok_line3     ; should overflow in second spaces-block and exit early
                ld      de,__t_tokens_vram_buf      ; the end HL points at the amount of spaces (implementation detail)
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf+128, "INCLUDE "
                __T_WORDCMP __t_tokens_vram_buf+254, ' ' ; upper byte is 0
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+255, __t_tokens_tok_line3+7
                __T_SETUP '4'
                ld      hl,__t_tokens_tok_line4     ; should overflow in second spaces-block and exit early
                ld      de,__t_tokens_vram_buf      ; the end HL points at the amount of spaces (implementation detail)
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf+252, "INC\0"
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+255, __t_tokens_tok_line4+7
                __T_SETUP '5'
                ld      hl,__t_tokens_tok_line5     ; tests the $0B..$1F space compression scheme + invalid $09 byte
                ld      de,__t_tokens_vram_buf
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf, "l1  BSRL   DE,    B                      ; de,b\0"
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+47, __t_tokens_tok_line5+17
                __T_SETUP '6'
                ld      hl,__t_tokens_tok_line6     ; test possibility of comment right after instruction token (w/o space)
                ld      de,__t_tokens_vram_buf
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf, "RET;\0"
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+4, __t_tokens_tok_line6+2
                __T_SETUP '.'
                ld      hl,__t_tokens_tok_line6b    ; explicit space between instruction and comment
                ld      de,__t_tokens_vram_buf
                call    detokenise
                __T_MEMCMP __t_tokens_vram_buf, "RET ;\0"
                __T_CHECK only_de_hl : dw __t_tokens_vram_buf+5, __t_tokens_tok_line6b+3

                ; check canary at end of output buffer
                __T_SETUP '!'
                __T_MEMCMP __t_tokens_vram_buf+$100, "$@$\0"
                __T_CHECK none

                ; TEST the tokenise tables are correctly sorted and unique by going one-by-one through all tokens
                __T_SETUP 'a'
                ld      ix,tokenise_is_ins
                ld      hl,InsTokenTable
                call    .checkAllTokens
                __T_CHECK only_zf
                __T_SETUP '2'
                ld      ix,tokenise_is_op
                ld      hl,TokenTable
                call    .checkAllTokens
                __T_CHECK only_zf

                ret

.checkAllTokens:
                ldi     b,(hl)
                inc     b
                ret     z
                dec     b
                ld      c,(hl)              ; expected BC (token:sub-index)
                ld      de,__t_tokens_vram_buf-1
.copyTokenL1:
                inc     hl
                ld      a,(hl)
                inc     de
                ld      (de),a
                rla
                jr      nc,.copyTokenL1
                xor     a
                ld      (de),a              ; terminate with null
                ld      de,__t_tokens_vram_buf  ; run tokenise
                push    hl, bc
                call    tokenise
                call    __t_cmp_ncf         ; check if found
                call    c,.printTableError
                pop     hl
                sbc     hl,bc
                pop     hl
                jr      z,.checkAllTokens
                ; print the problematic one
.printTableError:
                ld      a,2
                call    setColour
                ld      hl,__t_tokens_vram_buf
                call    printHL
                ld      a,7
                call    setColour
                ld      a,'!'
                call    printChar
                ld      a,(__t_test_name)
                jp      printChar

;;----------------------------------------------------------------------------------------------------------------------
; test values for particular tests

__t_tokens_txt_input3:  DB  "DB"  ; -> continue to form "DBCCF"
__t_tokens_txt_input2:  DB  'C'   ; -> continue to form "CCF"
__t_tokens_txt_input1:  DZ  "CF"
__t_tokens_txt_input4:  DZ  "CCF A"
__t_tokens_txt_input4b: DZ  "CCF ;" ; should not consume the space because ";" follows
__t_tokens_txt_input5:  DZ  "CCFA"
__t_tokens_txt_input6:  DZ  "CCF<"
__t_tokens_txt_inp_adc: DZ  "ADC"
__t_tokens_txt_inp_add: DZ  "ADD"
__t_tokens_txt_inp_byt: DZ  "BYTE"
__t_tokens_txt_inp_db:  DZ  "DB"
__t_tokens_txt_inp_deb: DZ  "DEFB"
__t_tokens_txt_inp_xor: DZ  "XOR"
__t_tokens_txt_inp_db2: DZ  "DB("           ; fails as instruction in current tokenizer (needs space|\0|;)

__t_tokens_tokentab:
.adc:           db $80,$00,"ADC"            ; $08 ending (as is)
.add:           db $81,$00,"ADD"            ; similar to ADC, just different third char
.byte:          db $8e,$02,"BYTE"           ; "DB" alias (sub-index 2)
.ccf:           db $85,$00,"CCF"            ; $0A ending (non-id char follows)
.db:            db $8e,$00,"DB"             ; "DB" has two aliases (this is main token)
.defb:          db $8e,$01,"DEFB"           ; "DB" alias (sub-index 1)
.xor:           db $ca,$00,"XOR"            ; last token in real table
.end:           db OP_UNKNOWN

__t_tokens_txt_line1    db  0               ; empty input line
__t_tokens_txt_line2    dz  "ccfo"          ; label starting with keyword
__t_tokens_txt_line3    dz  "ccf"           ; CCF instruction
__t_tokens_txt_line4    dz  ";                        ccf "      ; ccf in comment with 24 spaces, with 1 trailing (gets trimmed)
__t_tokens_txt_line5    dz  "lddrx a"       ; LDDRX (OP_LDRX 01) and A (OP_A)
__t_tokens_txt_line6    dz  "                                        nop"   ; tokens: $0A, 40, OP_NOP
__t_tokens_txt_line7    dz  "ld a,+($b<<1)+\";\""    ; tokens: OP_LD, OP_A, ",+($b", OP_SHL, "1)+\";\""
__t_tokens_txt_line7b   dz  "ld a,+\";\"+($b<<1)"    ; tokens: OP_LD, OP_A, ",+\";\"+($b", OP_SHL, "1)"
__t_tokens_txt_line8    dz  "l1 bsrl de,b ; de,b"   ; tokens: "l1 ", OP_BSRL, OP_DE, ',', OP_B, " ; de,b"
__t_tokens_txt_line9    dz  "db \"   \""            ; tokens: OP_DB, '"', $0A, 3, '"' (yes, space are compressed inside quotes by design)
__t_tokens_txt_line10   dz  "ret;"                  ; tokens: OP_RET, ';'
__t_tokens_txt_line10b  dz  "ret ;"                 ; tokens: OP_RET, " ;"
__t_tokens_txt_line11   dz  "  ld a,\""             ; tokens: $1F, OP_LD, OP_A, ',"' -> reports missing closing double-quote
__t_tokens_txt_line11b  dz  "  ld b,'x"             ; tokens: $1F, OP_LD, OP_B, ','x' -> reports missing closing single-quote

__t_tokens_buf_out      db  "_______________"       ; 15 bytes required by the longest line8
.can                    dz  "$@$"

__t_tokens_vram_buf     equ $4000+$1400     ; right after the tilemode map data (must be 256B aligned)

__t_tokens_tok_line1    dz  OP_CCF
__t_tokens_tok_line2    dz  "l1", $0A, 2, OP_BSRL, OP_DE, ',', $0A, 2, OP_B, " ; de,b"
__t_tokens_tok_line3    dz  $0A, 128, OP_INCLUDE, $01, $0A, 128, OP_B    ; should overflow in second spaces-block
__t_tokens_tok_line4    dz  $0A, 252, OP_INCLUDE, $01, $0A, 128, OP_B    ; should overflow in OP_INCLUDE
__t_tokens_tok_line5    dz  "l1", $1F, OP_BSRL, $1E, OP_DE, ',', $1D, OP_B, $0B, ';', $09, " de,b"  ; $09 is invalid "token" byte
    ; should expand as "l1  BSRL   DE,    B                      ; de,b"
__t_tokens_tok_line6    dz  OP_RET, ";"     ; should expand as "ret;" without implicit space
__t_tokens_tok_line6b   dz  OP_RET, " ;"    ; should expand as "ret ;" (without implicit space)

                display "[TEST RUN] tests: tokens.s                     Space: ",/A,$-__test_tokens_s
