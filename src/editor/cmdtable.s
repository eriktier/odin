;;----------------------------------------------------------------------------------------------------------------------
;; Editor's command table
;;----------------------------------------------------------------------------------------------------------------------

;; This table maps keycodes to functions

CmdTable00:
                ; 00
                dw      cmdDoNothing            ; 
                dw      editorExit              ; EDIT
                dw      cmdDoNothing            ; CAPS LOCK
                dw      editorTab               ; TRUE VIDEO
                dw      cmdDoNothing            ; INV VIDEO
                dw      editorLeft              ; LEFT
                dw      editorDown              ; DOWN
                dw      editorUp                ; UP
                dw      editorRight             ; RIGHT
                dw      cmdDoNothing            ; GRAPH
                dw      editorBackspace         ; DELETE
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorNewLine           ; ENTER
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; 10
                dw      cmdDoNothing            ; SYM+W
                dw      cmdDoNothing            ; SYM+E
                dw      cmdDoNothing            ; SYM+I
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorSpace             ; BREAK
                dw      cmdDoNothing            ; SYM+SPACE
                dw      cmdDoNothing            ; SHIFT+ENTER
                dw      cmdDoNothing            ; SYM+ENTER
                dw      cmdDoNothing            ; 

CmdTable80:
                ; 80
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      editorNewLineAbove      ; EXT+ENTER
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; 90
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; A0
                dw      cmdDoNothing            ; EXT+SPACE
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; B0
                dw      editorDelete            ; EXT+0
                dw      editorSwitch            ; EXT+1
                dw      cmdDoNothing            ; EXT+2
                dw      editorStartDoc          ; EXT+3
                dw      editorEndDoc            ; EXT+4
                dw      editorHome              ; EXT+5
                dw      editorPageDown          ; EXT+6
                dw      editorPageUp            ; EXT+7
                dw      editorEnd               ; EXT+8
                dw      cmdDoNothing            ; EXT+9
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; C0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; D0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 

                ; E0
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; EXT+A
                dw      cmdDoNothing            ; EXT+B
                dw      editorCopyLine          ; EXT+C
                dw      editorDupLine           ; EXT+D
                dw      editorDeleteToEnd       ; EXT+E
                dw      cmdDoNothing            ; EXT+F
                dw      cmdDoNothing            ; EXT+G
                dw      cmdDoNothing            ; EXT+H
                dw      cmdDoNothing            ; EXT+I
                dw      cmdDoNothing            ; EXT+J
                dw      cmdDoNothing            ; EXT+K
                dw      cmdDoNothing            ; EXT+L
                dw      cmdDoNothing            ; EXT+M
                dw      cmdDoNothing            ; EXT+N
                dw      printChar.overwrite     ; EXT+O

                ; F0
                dw      cmdDoNothing            ; EXT+P
                dw      cmdDoNothing            ; EXT+Q
                dw      cmdDoNothing            ; EXT+R
                dw      editorSave              ; EXT+S
                dw      cmdDoNothing            ; EXT+T
                dw      cmdDoNothing            ; EXT+U
                dw      editorPasteLine         ; EXT+V
                dw      editorCloseDoc          ; EXT+W
                dw      editorCutLine           ; EXT+X
                dw      cmdDoNothing            ; EXT+Y
                dw      cmdDoNothing            ; EXT+Z
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
                dw      cmdDoNothing            ; 
