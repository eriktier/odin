;;----------------------------------------------------------------------------------------------------------------------
;; Math routines
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; mul_16_8_24
;; Multiple a 16-bit value by an 8-bit value to get a 24-bit value
;;
;; Input:
;;      HL = 16-bit value
;;      A = 8-bit value
;;
;; Output:
;;      EHL = 24-bit value
;;

;;      HL
;;       A
;;     ---
;;     DEC

mul_16_8_24:
                ld      b,a
                ld      e,a
                ld      d,l
                mul     de      ; DE = L*A
                ld      c,e     ; C = LSB, D = carry
                ld      a,d
                ld      e,b
                ld      d,h
                mul     de
                add     e       ; answer is DAC
                ld      e,d
                ld      h,a
                ld      l,c
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; mul_32_8_40
;; Multiple a 32-bit value by an 8-bit value to get a 24-bit value
;; Taken from Z88DK source code
;;
;; Input:
;;      DEHL = 32-bit value
;;      A = 8-bit value
;;
;; Output:
;;      ADEHL = 40-bit product
;;      CF = 0
;;
;; Affects:
;;      AF,BC,AF'
;; 

mul_32_8_40:
                ; Input and result is now BCHL and ABCHL
                ld      bc,de           ; BC = DE because we use DE for multiplication

                ; First multiply (L*A)
                ld      e,l
                ld      d,a
                mul                     ; E = result, D = carry

                exa
                ld      l,e
                ld      a,d             ; result = xxxxL, A' = carry
                exa

                ; Second multiply (H*A)
                ld      e,h
                ld      d,a
                mul                     ; E = result, D = carry

                exa
                add     a,e             ; Add carry
                ld      h,a             ; result = xxxHL
                ld      a,d             ; next carry
                exa

                ; Third multiply (E*A)
                ld      e,c
                ld      d,a
                mul                     ; E = result, D = carry

                exa
                adc     a,e             ; Add carry
                ld      c,a             ; result = xxCHL
                ld      a,d             ; next carry
                exa

                ; Fourth multiply (D*A)
                ld      e,b
                ld      d,a
                mul     de              ; E = result, D = carry

                exa
                adc     a,e             ; Add carry
                ld      b,a             ; result = xBCHL

                ;ld      a,d
                ;adc     a,0             ; final carry
                adc     a,d
                sub     b

                ; Relocate BC->DE
                ld      de,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; unpackD24
;; Convert a 24-bit value into BCD
;;
;; Input:
;;      EHL = 24-bit value
;;
;; Output:
;;      DEHL = BCD of value in 8-digit form
;;
;; Affects:
;;      AF, DE, HL
;;

unpackD24:
                push    bc,ix
                ld      c,e
                push    hl
                pop     ix              ; CIX = 24-bit value
                ld      hl,1
                ld      d,h
                ld      e,h
                ld      b,24            ; 24 bits to process

.l1:
                add     ix,ix
                rl      c               ; Push top bit of CIX into carry
                jr      c,.nextbit
                djnz    .l1             ; Find highest 1-bit

                ; All bits are 0
                res     0,l             ; LSBit not 1
                pop     ix,bc
                ret

.l2:
                ld      a,l
                add     a,a
                daa
                ld      l,a
                ld      a,h
                adc     a,a
                daa
                ld      h,a
                ld      a,e
                adc     a,a
                daa
                ld      e,a
                ld      a,d
                adc     a,a
                daa
                ld      d,a
                add     ix,ix
                rl      c
                jr      nc,.nextbit
                set     0,l
.nextbit        djnz    .l2
                pop     ix,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; mul_16_16_32
;; Multiply two 16-bit values to get a 32-bit value
;;
;; Input:
;;      HL & HL' = 16 bit values (X&Y)
;;
;; Output:
;;      DEHL = 32-bit product
;;      CF = 0
;;
;; Uses:
;;      AF, BC, DE, HL, BC', DE', HL'
;;
;; Taken from Z88DK source code
;;

mul_16_16_32:
                ; HL & DE become the values
                push    hl
                exx
                pop     de

                ; HL = X
                ; DE = Y

mul_DE_HL_DEHL:
                ld      b,l     ; B = X0
                ld      c,e     ; C = Y0
                ld      e,l     ; E = X0
                ld      l,d     ; L = Y1
                push    hl
                ld      l,c     ; L = Y0

                ; BC = X0 Y0
                ; DE = Y1 X0
                ; HL = X1 Y0
                ; STACK = X1 Y1

                mul             ; Y1*X0
                ex      de,hl
                mul             ; X1*Y0

                xor     a
                add     hl,de   ; Sum cross products
                adc     a,a

                ld      e,c     ; X0
                ld      d,b     ; Y0
                mul             ; X0*Y0

                ld      b,a
                ld      c,h

                ld      a,d
                add     a,l
                ld      h,a
                ld      l,e

                pop     de
                mul

                ex      de,hl
                adc     hl,bc
                ex      de,hl

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; mul_32_32_32
;; Multiply 2 unsigned 32-bit values into a 32-bit product.
;;
;; Input:
;;      DEHL = value 1
;;      DE'HL' = value 2
;;
;; Output:
;;      DEHL = 32-bit product
;;      CF = 0
;;
;; Uses:
;;      AF, BC, DE, HL, BC', DE', HL'
;;
;; Taken from Z88DK source code
;;

mul_32_32_32:
                ; If DE and DE' are both 0, use the 16x16 version.
                xor     a
                or      e
                or      d

                exx
                or      e
                or      d
                jr      z,mul_16_16_32

                push    hl
                exx
                ld      c,l
                ld      b,h
                pop     hl
                push    de
                ex      de,hl
                exx
                pop     bc

                ; DEDE' = 32-bit X
                ; BCBC' = 32-bit Y
                push    de      ; X3 X2
                exx
                push    bc      ; Y1 Y0
                push    de      ; X1 X0
                exx
                push    bc      ; Y3 Y2

                ; Save material for the byte p2 = x2*y0 + x0*y2 + x1*y1 + p1 carry

                ld      h,e
                ld      l,c
                push    hl      ; X2 Y2

                exx             ; Working in low order bytes
                ld      h,e
                ld      l,c
                push    hl      ; X0 Y0

                ld      h,d
                ld      l,b
                push    hl      ; X1 Y1

                ld      h,d     ; X1
                ld      d,b     ; Y1
                ld      l,c     ; Y0
                ld      b,e     ; X0

                ; BC = X0 Y0
                ; DE = Y1 X0
                ; HL = X1 Y0
                ; STACK = X1 Y1

                mul             ; Y1*X0
                ex      de,hl
                mul             ; X1*Y0

                xor     a
                add     hl,de
                adc     a,a

                ld      e,c     ; X0
                ld      d,b     ; Y0
                mul             ; Y0*X0

                ld      b,a
                ld      c,h

                ld      a,d
                add     a,l
                ld      h,a
                ld      l,e     ; LSW in HL p1 p0

                pop     de
                mul             ; X1*Y1

                ex      de,hl
                adc     hl,bc   ; HL = interim MSW p3 p2
                ex      de,hl   ; DEHL = end of 16x16->32

                push    de      ; Stack interim p3 p2

                ; Continuing doing the p2 byte

                exx             ; Now we're working in the high-order bytes
                                ; DE'HL' = end of 16x16->32
                pop     bc      ; Stack interim p3 p2

                pop     hl      ; X0 Y0
                pop     de      ; X2 Y2
                ld      a,h
                ld      h,d
                ld      d,a
                mul             ; X0*Y2
                ex      de,hl
                mul             ; X2*Y0

                add     hl,bc
                add     hl,de
                ld      b,h
                ld      c,l

                ; Start doing the p3 byte

                pop     hl      ; Y3 Y2
                pop     de      ; X1 X0
                ld      a,h
                ld      h,d
                ld      d,a
                mul             ; Y3*X0
                ex      de,hl
                mul             ; X1*Y2

                ld      a,b
                add     a,e
                add     a,l

                pop     hl      ; Y1 Y0
                pop     de      ; X3 X2
                ld      b,h
                ld      h,d
                ld      d,b
                mul             ; Y1*X2
                ex      de,hl
                mul             ; X3*Y0

                add     a,l
                add     a,e
                ld      b,a

                push    bc

                exx
                pop     de
                xor     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; divs_32_32_32
;; Division of two 32-bit signed numbers to create 32-bit quotient (unsigned) and 32-bit remainder (signed).
;;
;; Input:
;;      DE'HL' = 32-bit dividend
;;      DEHL = 32-bit divisor
;;
;; Output:
;;      Success:
;;              CF = 0
;;              DEHL = 32-bit quotient
;;              DE'HL' = 32-bit remainder
;;
;;      Divide by zero:
;;              CF = 1
;;
;; Uses:
;;      AF, BC, DE, HL, BC', DE', HL'
;;
;; Taken from Z88DK source code
;;

divs_32_32_32:
                ld      a,d
                or      e
                or      h
                or      l
                scf
                ret     z

                ; A = (A/B)*B + A%B

                ld      a,d

                exx

                ld      b,d             ; B = MSB of dividend
                ld      c,a             ; C = MSB of divisor

                push    bc              ; Save sign info
                bit     7,d
                call    nz,neg_dehl     ; Take absolute value of divisor

                exx

                bit     7,d
                call    nz,neg_dehl

                call    divu_32_32_32_no0

                ; DEHL = unsigned quotient
                ; DE'HL = unsigned remainder

                pop     bc              ; BC = sign info
                ld      a,b
                xor     c
                call    m,neg_dehl
                bit     7,b
                ret     z               ; If dividend > 0

                exx
                call    neg_dehl        ; Negate remainder
                exx

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; neg_dehl
;; Negate a 32-bit word held in DEHL
;;
;; Input:
;;      DEHL = value
;;
;; Output:
;;      DEHL = -value
;;
;; Taken from Z88DK source code
;;

invert          macro   reg
                ld      a,reg
                cpl
                ld      reg,a
                endm

neg_dehl:
                invert  l
                invert  h
                invert  e
                invert  d
                inc     l
                ret     nz
                inc     h
                ret     nz
                inc     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; divu_32_32_32
;; Unsigned 32-bit division
;;
;; Input:
;;      DE'HL' = 32-bit dividend
;;      DEHL = 32-bit divisor
;;
;; Output:
;;      Success:
;;              CF = 0
;;              DEHL = 32-bit quotient
;;              DE'HL' = 32-bit remainder
;;
;;      Divide by zero:
;;              CF = 1
;;
;; Taken from Z88DK source code
;;

divu_32_32_32:
                ld      a,d
                or      e
                or      h
                or      l
                scf
                ret     z
divu_32_32_32_no0:
                xor     a
                push    hl
                exx
                ld      c,l
                ld      b,h
                pop     hl
                push    de 
                ex      de,hl
                ld      l,a
                ld      h,a
                exx
                pop     bc
                ld      l,a
                ld      h,a

                ; BCBC' = 32-bit dividend
                ; DEDE' = 32-bit divisor
                ; HLHL' = 0

                ld      a,b
                ld      b,32
.l0:
                exx
                rl      c
                rl      b
                exx
                rl      c
                rla

                exx
                adc     hl,hl
                exx
                adc     hl,hl

                exx
                sbc     hl,de
                exx
                sbc     hl,de
                jr      nc,.l1

                exx
                add     hl,de
                exx
                adc     hl,de
.l1:
                ccf
                djnz    .l0

                exx
                rl      c
                rl      b
                exx
                rl      c
                rla

                ; Quotient = ACBC'
                ; Remainder = HLHL'
                push    hl
                exx
                pop     de
                push    bc
                exx
                pop     hl
                ld      e,c
                ld      d,a
                ret

;;----------------------------------------------------------------------------------------------------------------------
