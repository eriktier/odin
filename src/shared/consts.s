;;----------------------------------------------------------------------------------------------------------------------
;; Odin's constants
;;----------------------------------------------------------------------------------------------------------------------

kScreenWidth    equ     80
kScreenHeight   equ     32
kScreenTop      equ     $4000

;                        GRB H GRB
kInkNormal      equ     %000'1'111
kInkError       equ     %000'1'010
kInkTitle       equ     %000'0'100
kInkResponse    equ     %000'0'110
kInkSuccess     equ     %000'0'100
kInkComment     equ     %000'1'000

; Editor colours
kInkStatus      equ     %010'0'000
kInkFileName    equ     %010'0'110
kInkTilde       equ     %000'0'001
kInkLongLine    equ     %000'0'010

; Editor switch menu colours
kInkSwitch      equ     %001'0'111
kInkSwitchCur   equ     %100'1'100

; LS command
kInkDirectory   equ     %000'0'100
kInkOdinFile    equ     %000'0'111
kInkNormalFile  equ     %000'1'000

; M command
kInkAddr24      equ     %000'0'100
kInkAddr16      equ     %000'1'100
kInkBytes       equ     %000'1'000

