;;----------------------------------------------------------------------------------------------------------------------
;; String processing routines
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; isSpace macro
;;
;; Input:
;;      A = character
;;
;; Output:
;;      CF = 1 if is whitespace
;;

isSpace         macro
                cp      $21
                endm

;;----------------------------------------------------------------------------------------------------------------------
;; isDigit
;; Returns CF = 0 if A is a digit
;;

isDigit:
                cp      '0'
                ret     c
                cp      '9'+1
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; isHexDigit
;; Returns CF = 0 if A is a hexadecimal digit
;;

isHexDigit:
                cp      'A'
                jr      c,isDigit       ; 0..'A'-1 -> isDigit check
                cp      'a'
                jr      c,.isUpperAf    ; 'A'..'a'-1 -> isUpperAf check
                cp      'f'+1           ; 'a'..255 -> check for a-f range
                ccf
                ret
.isUpperAf:
                cp      'F'+1           ; A-F range check
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; isIdentChar
;;
;; Input:
;;      A = character
;;
;; Output:
;;      CF = 0 if character is 0-9,a-z,A-Z or _ or .
;;

isIdentChar:
                cp      '.'
                ret     z
                cp      '_'
                ret     z           ; CF=0 from sub - extra case for '_'
                cp      'A'
                jr      c,isDigit   ; 0..'A'-1 handled by isDigit
                ; Continues into isAlpha (with 'A'..255, without '_')

;;----------------------------------------------------------------------------------------------------------------------
;; isAlpha
;;
;; Input:
;;      A = character
;;
;; Output:
;;       CF = 0 if character is a-z or A-Z
;;

isAlpha:
                cp      'a'
                jr      c,isUpperAlpha
                cp      'z'+1
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; isUpperAlpha
;;
;; Input:
;;      A = character
;;
;; Output:
;;      CF = 0 if character is A-Z
;;

isUpperAlpha:
                cp      'A'
                ret     c
                cp      'Z'+1
                ccf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; upperCase
;;
;; Input:
;;      A = character
;;
;; Output:
;;      A = upper case character
;;

upperCase:
                cp      'a'
                ret     c
                cp      'z'+1
                ret     nc
                sub     32
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; makePrintable
;;
;; Input:
;;      A = byte value
;;
;; Output:
;;      A = ASCII $20..$7F character, or '.' character for other input values
;;

makePrintable:
                cp      $80
                jr      nc,.bad_char
                cp      $20
                ret     nc
.bad_char:
                ld      a,'.'
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strSkipWS
;; Advance pointer until it passes all whitespace or hits a null terminator
;;
;; Input:
;;      HL = input
;;
;; Output:
;;      HL = Input past any whitespace
;;      A = next non-whitespace character or 0
;;

strSkipWS_loop:
                inc     hl
strSkipWS:
                ld      a,(hl)
                dec     a                   ; adjust char value so 0 becomes 255
                cp      ' '
                jr      c,strSkipWS_loop    ; original chars 1..' ' are looping, 0 or space+ pass through
                inc     a                   ; restore original char value
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strFindWS
;; Advance pointer until it passes all non-whitespace or hits a null terminator
;;
;; Input:
;;      HL = Input
;;
;; Output:
;;      HL = Points to first whitespace in input or null terminator
;;

strFindWS:
                ld      a,(hl)
                cp      $21
                ret     c                   ; 0 or whitespace
                inc     hl
                jr      strFindWS

;;----------------------------------------------------------------------------------------------------------------------
;; strDec
;;
;; Input:
;;      HL = Input buffer
;;
;; Output:
;;      CF=1 on an error (no valid number in buffer)
;;      DEHL = result
;;      BC = points after number
;;

strDec:
                call    strSkipWS
                ; A is first non-space char or zero - process it as first digit
                sub     '0'
                cp      10
                ccf
                ret     c               ; Not a valid digit, fail

                ; A = first digit
                ; HL = character pointer after first digit
                ld      de,0
                ld      bc,hl           ; BC = char*
                ld      h,d
                ld      l,a             ; DEHL = total so far (i.e. the first digit)

.l1:
                ; Get next digit
                inc     bc
                ld      a,(bc)          ; Get next digit
                sub     '0'
                cp      10
                ret     nc              ; End of digits
                push    af
                ld      a,10

                ; DEHL = DEHL * 10
                push    bc
                call    mul_32_8_40
                pop     bc

                ; Add in the digit
                pop     af
                add     a,l
                ld      l,a
                jr      nc,.l1
                inc     h
                jr      nz,.l1
                inc     de
                jr      .l1

;;----------------------------------------------------------------------------------------------------------------------
;; strlen
;; Return the length of the string
;;
;; Input:
;;      HL = buffer
;;
;; Output:
;;      BC = length

strlen:
                push    af,hl
                xor     a
                ld      b,a
                ld      c,a
                cpir        ; BC = - length of string, including null-terminator
                ; do BC = ~BC <=> BC = (-BC)-1 <=> strlen without null-terminator (!)
                ld      a,c
                cpl
                ld      c,a
                ld      a,b
                cpl
                ld      b,a
                pop     hl,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strEnd
;; Return the address of the end of string
;;
;; Input:
;;      HL = string
;;
;; Output:
;;      HL = end of string.  (HL) == 0.
;;

strEnd:
                push    af
                xor     a
                dec     hl
.l1:            inc     hl
                cp      (hl)
                jp      nz,.l1
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strcpy
;; Copy one string into the buffer of another
;;
;; Input:
;;      HL = source string
;;      DE = destination buffer
;;
;; Output:
;;      HL = end of source string pointing after null terminator
;;      DE = destination buffer after the string that's just been copied
;;      ZF = 1
;;      CF = 0
;;

strcpy:
                push    bc
                xor     a
.l1             cp      (hl)
                ldi
                jp      nz,.l1
                pop     bc      ; push/pop BC becomes faster for 3+ char strings, for 0..3 char `inc bc` would be better
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strSliceCopy
;; Copy a slice of a string (defined between HL and BC) to a buffer
;;
;; Input:
;;      HL = source string
;;      BC = end of string
;;      DE = destination buffer
;;
;; Output:
;;      DE = destination buffer after the string that's just been copied
;;      A = character at (BC) (first omitted character from source string)
;;
;; Affects:
;;      HL
;;

strSliceCopy:
                ld      a,(bc)
                push    af
                xor     a
                ld      (bc),a
                call    strcpy
                pop     af
                ld      (bc),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; strcmp
;; Test a string until a difference occurs.  A will be the difference (1st different char - 2nd different char).
;; This means that A < 0 (signed 8-bit) then first string comes before second string.  If A == 0 (or Z flag is set), strings are the
;; same up until they are terminated (with a 0).
;;
;; Input:
;;      DE = 1st string (signed characters)
;;      HL = 2nd string (signed characters)
;;
;; Output:
;;      A = difference of first different character (0 if strings are the same)
;;      ZF = 1 if strings are the same
;;
 
strcmp:
                push    de
                push    hl
.l1             ld      a,(de)
                sub     (hl)            ; A = difference (or 0 if both are same)
                jr      nz,.no_match
                or      (hl)
                inc     de
                inc     hl
                jp      nz,.l1
.no_match       pop     hl
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
