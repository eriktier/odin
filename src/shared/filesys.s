;;----------------------------------------------------------------------------------------------------------------------
;; File System management
;;----------------------------------------------------------------------------------------------------------------------

FILE            macro   func

                call    pageDivMMC
                dos     func
                call    pageOutDivMMC

                endm

;;----------------------------------------------------------------------------------------------------------------------
;; Variables
;;

FileSysHandle   db      0               ; Current file handle created by last fOpen
FileSysStat     ds      11              ; Space to store file information

FileBufferLen   db      0               ; Amount of data read into file buffer

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenFile
;; Open a file based on a given mode.
;;
;; Input:
;;      FileName buffer = name of file to open.
;;      B = open mode
;;
;; Output:
;;      CF = 1 if error occurs.
;;      HL = FileBuffer
;;

fOpenFile:
                push    af,bc,de,hl,ix

                xor     a
                FILE    M_GETSETDRV
                jr      c,fError

                ld      hl,FileName
                push    hl
                pop     ix
                FILE    F_OPEN
                jr      c,fError
                ld      (FileSysHandle),a
                xor     a
                ld      (FileBufferLen),a
                pop     ix,hl
                ld      hl,FileBuffer           ; Set up HL for streamed read/write if that's what we want
                jr      fDoneHL

fDone:
                pop     ix
                pop     hl
fDoneHL:
                pop     de
fDoneHLDE:
                pop     bc,af
                and     a
                ret

fError:
                pop     ix,hl,de,bc,af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenLoad
;; Open a file, whose filename is stored in FileName buffer.  FileSysHandle will store the handle.
;;
;; Output:
;;      CF = 1 if error occurred
;;

fOpenLoad:
                push    bc
                ld      b,FA_READ | FA_OPEN_EXISTING
                call    fOpenFile
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenSave
;; Open a file for saving to.  The filename will be in the FileName buffer.  FileSysHandle will store the handle.
;;
;; Output:
;;      CF = 1 if error occurred
;;

fOpenSave:
                push    bc
                ld      b,FA_WRITE | FA_CREATE_NEW
                call    fOpenFile
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fOpenAppend
;; Open a file for saving to but keep the previous contents.  The filename will be in the FileName buffer.
;; FileSysHandle will store the handle.
;;
;; Output:
;;      CF = 1 if error occurred
;;

fOpenAppend:
                push    bc
                ld      b,FA_WRITE | FA_OPEN_EXISTING
                call    fOpenFile
                jr      c,.error

                ; Seek to the end
                call    fGetSize
                call    fSeek

.error:
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fGetSize
;; Get the size of the currently opened file.
;;
;; Output:
;;      DEHL = file size
;;

fGetSize:
                push    af,bc,de,hl,ix
                ld      a,(FileSysHandle)
                ld      hl,FileSysStat
                push    hl
                pop     ix
                FILE    F_FSTAT
                jr      c,fError

                pop     ix,hl,de
                ld      hl,(FileSysStat+7)
                ld      de,(FileSysStat+9)
                jr      fDoneHLDE

;;----------------------------------------------------------------------------------------------------------------------
;; fSeek
;; See to a new position in the file.
;;
;; Input:
;;      DEHL = file position to seek to
;;

fSeek:
                push    af,bc,de,hl,ix
                ld      a,(FileSysHandle)
                ld      bc,de
                ld      de,hl           ; BCDE = file position
                ld      l,0
                push    hl
                pop     ix
                FILE    F_SEEK
                jp      fDone

;;----------------------------------------------------------------------------------------------------------------------
;; fClose
;; Close the file we currently have opened
;;

fClose:
                ld      a,(FileSysHandle)
                FILE    F_CLOSE
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fRead
;; Read a chunk of data from a file.  Do not use in conjunction with fReadByte and this uses the buffering system.
;;
;; Input:
;;      HL = address to write data to
;;      BC = number of bytes to read
;;
;; Output:
;;      BC = number of bytes actually read
;;      CF = 1 if error occurs
;;

fRead:
                or      a               ; enforce CF=0 after `pop af`
                push    af,de,hl
                ld      a,(FileSysHandle)
                FILE    F_READ
                pop     hl,de
                jr      c,.error
                pop     af
                ret
.error:
                pop     af
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fWrite
;; Read a chunk of data to a file.  Do not use in conjunction with fWriteByte and this uses the buffering system.
;;
;; Input:
;;      HL = address to write data from
;;      BC = number of bytes to read
;;
;; Output:
;;      CF = 1 if error occurs
;;

fWrite:
                push    af,bc,de,hl,ix
                ld      a,(FileSysHandle)
                push    hl
                pop     ix
                FILE    F_WRITE
                jp      c,fError
                jp      fDone

;;----------------------------------------------------------------------------------------------------------------------
;; fReadByte
;; Read a single byte from the currently opened file.
;;
;; Input:
;;      HL = position in file buffer
;;
;; Output:
;;      A = byte
;;      HL = next position in file buffer
;;      CF = 1 if error occurred
;;      ZF = 1 if no more data
;;

fReadByte:
                ld      a,(FileBufferLen)
                cp      l                       ; also enforces CF = 0 for buffer read
                jr      nz,.read

                ; no data, try to read 255 more
                push    bc
                ld      bc,255                  ; Read in 255 bytes
                ld      l,b                     ; HL = FileBuffer (it's 256B aligned)
                call    fRead
                ld      a,c
                ld      (FileBufferLen),a       ; Write number of bytes left
                pop     bc
                ret     c                       ; CF=1 report error!
                and     a                       ; Any bytes read?
                ret     z                       ; No - no more data!

.read:
                ld      a,(hl)
                inc     l                       ; ZF = 0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; fWriteByte
;; Write a single byte to the currently opened file.
;;
;; NOTE: Do not forget to call fFlush before fClose!
;;
;; Input:
;;      HL = position in buffer
;;      A = byte to write
;;
;; Output:
;;      HL = next position in file buffer to write
;;      CF = 1 if error occurs
;;

;; currently unused - if needed, uncomment (+ re-test thoroughly, especially edge-case like fFlush on empty buffer)
; fWriteByte:
;                 or      a               ; CF = 0 for non-write call
;                 ld      (hl),a
;                 inc     l
;                 ret     nz
;                 push    bc
;                 ld      bc,256
; .write:
;                 ld      hl,FileBuffer
;                 call    fWrite
;                 pop     bc
;                 ret

;;----------------------------------------------------------------------------------------------------------------------
;; fFlush
;; Write all bytes still in the file buffer to the file
;;
;; Input:
;;      L = number of bytes to write.
;;
;; Output:
;;      HL = next position in file buffer to write
;;      CF = 1 if error occurs
;;

; fFlush:
;                 push    bc
;                 ld      c,l
;                 ld      b,0
;                 jr      fWriteByte.write

;;----------------------------------------------------------------------------------------------------------------------
;; fGetCwd
;; Get current folder into FileName buffer
;;
;; Output:
;;      CF = 1 if error occurs
;;

fGetCwd:
                push    af,hl

                ld      a,'*'
                ld      hl,FileName
                FILE    F_GETCWD

.end:
                pop     hl,af
                ret
