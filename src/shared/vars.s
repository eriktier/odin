;;----------------------------------------------------------------------------------------------------------------------
;; Global variables used by Odin
;;----------------------------------------------------------------------------------------------------------------------

InputBuffer     ds      80
                db      0               ; Terminate input in case it's 80 characters long.

FileName:       ds      300,0
Clipboard:      ds      kScreenWidth,0

;;----------------------------------------------------------------------------------------------------------------------
;; Symbol table shared variables

NumSymPages     db      0               ; Number of pages allocated for symbols
NumSyms         dw      0               ; Index of next symbol
SymPages        ds      8               ; Up to 64K of symbols (each one taking 32 bytes)

;;----------------------------------------------------------------------------------------------------------------------
;; Assembler shared information

CodePages       ds      4               ; Pages allocated or initial MMU state
StartAddress    dw      0

                ; Current MMU state during assembly - $ff = can't assemble here
MMUState        ds      9,$ff

