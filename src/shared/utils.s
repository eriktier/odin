;;----------------------------------------------------------------------------------------------------------------------
;; Various utilities used by Odin
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; max

max:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       HL = maximum value
        ;       DE = minimum value
        ;       CF = 1 if DE was maximum
        ;
                and     a
                sbc     hl,de
                add     hl,de           ; Restore HL (does produce same CF as sbc before)
                ret     nc              ; HL >= DE, CF=0
                ex      de,hl           ; CF=1 -> HL < DE -> Choose DE!
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit compare

compare16:
        ; Input:
        ;       HL = 1st value
        ;       DE = 2nd value
        ; Output:
        ;       CF, ZF = results of comparison:
        ;
        ;               CF      ZF      Result
        ;               -----------------------------------
        ;               0       0       HL > DE
        ;               0       1       HL == DE
        ;               1       0       HL < DE
        ;               1       1       Impossible
        ;
                and     a
                sbc     hl,de
                add     hl,de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; alignUp8K
;; align a 16-bit value up to the nearest 8K
;;
;; Input:
;;      HL = value
;;
;; Output:
;;      HL = aligned value

alignUp8K:
                push    af
                xor     a
                cp      l
                ld      l,a
                adc     a,h
                add     a,$1f
                and     -$20
                ld      h,a             ; HL = (HL+$2000-1) & -$2000  (-$2000 = $E000)
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; waitNoKey
;; Wait for no key to be pressed
;;

waitNoKey:
                xor     a
                in      a,($fe)
                cpl
                and     $1f                     ; We need lower 5 bits to be 00000 (not pressed)
                jr      nz,waitNoKey
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; waitAnyKey
;; Wait for any key to be pressed
;;

waitAnyKey:
                xor     a
                in      a,($fe)
                cpl
                and     $1f
                jr      z,waitAnyKey
                call    flushKeys
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; breakPressed
;; Return ZF = 1 if break is pressed
;;
;; Output:
;;      ZF = 1 if break is pressed
;;      CF = 1 if only shift is pressed

breakPressed:
                ld      a,$fe           ; Key row SHIFT-V
                in      a,($fe)
                and     1               ; ZF = 1 if SHIFT is pressed
                ret     nz              ; Return with CF = 0 if shift is not pressed
                ld      a,$7f           ; Key row SPACE-B
                in      a,($fe)
                and     1               ; ZF = 1 if SPACE is pressed
                ret     z               ; Return with CF = 0 if BREAK is pressed
                scf                     ; Only shift was pressed (not space) so set CF
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; breakPause
;; This routine waits for a key and returns ZF = 1 if BREAK was pressed, otherwise ZF = 0.  It will also make sure
;; that the routine doesn't exist if only SHIFT is pressed.
;;

breakPause:
                call    waitKey
                cp      VK_BREAK
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; hexChar
;; Convert a hex ASCII character to a nybble.
;;
;; Input:
;;      A = ASCII character (0-9, a-f, A-F)
;;
;; Output:
;;      A = nybble
;;

;; A    0A
;; a    2A

hexChar:
                sub     '0'             ; 0-9 if char is 0-9, $11-$16 if A-F, $31-$36 if a-f
                cp      10
                ret     c               ; Done if 0-9

                sub     7               ; $0A-$0F if A-F, $2A-$2F if a-f
                and     $0F
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; distance
;; Calculate the distance between HL and DE
;;
;; Affects:
;;      AF
;;

distance:
                and     a
                sbc     hl,de
                ret     nc              ; HL = distance
                
negate16:
                xor     a
                sub     l
                ld      l,a
                sbc     a,a
                sub     h
                ld      h,a
                ret



;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
