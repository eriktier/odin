;;----------------------------------------------------------------------------------------------------------------------
;; Paging system
;;----------------------------------------------------------------------------------------------------------------------

SharedCode:     db      0               ; Page for shared code at $c000-$dfff
MonitorCode:    db      0               ; Page for monitor code at $e000-$ffff
EditorCode:     db      0               ; Page for editor code at $e000-$ffff
AsmCode:        db      0               ; Page for assembler code at $e000-$ffff
DbgCode:        db      0               ; Page for debugger code at $0000-$1fff
Data:           db      0               ; Page for debugger's data at $2000-$3fff

NUM_CODE_PAGES  equ     $-SharedCode    ; Number of entries above

DivMMCPage:     db      0
PageState       ds      32              ; One bit per page assembled too

NexMode         db      0               ; 1 if OPT NEX has been run at least once
TopPage         db      $df             ; Index of highest page in system - TODO: calculate this on set up

; Most of the page swaps are in slot 4, so this has a special entry point to save bytes
readPage4:
                ld      a,4
readPage:
                add     a,REG_MMU0
                push    bc
                ld      bc,IO_REG_SELECT
                out     (c),a
                inc     b
                in      a,(c)
                pop     bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDoc
;; Page the document in  MMU0-5

pageDoc:
                push    af,hl
                xor     a
                out     ($e3),a
                ld      hl,MainDoc.Pages
.l1:
                ld      a,REG_MMU0 - low MainDoc.Pages
                add     a,l                                 ; A = REG_MMU0 .. REG_MMU5 based on HL address
                ld      (.page),a
                cp      REG_MMU5
                ldi     a,(hl)
.page+2         page    0,a
                jr      nz,.l1
                pop     hl,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDivMMC
;; Page in the DivMMC

pageDivMMC:
                push    af
                ld      a,(DivMMCPage)
                out     ($e3),a
                page    0,$ff
                page    1,$ff
                page    2,10
                page    3,11
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageOutDivMMC
;; Ensure that DivMMC RAM and esxDOS RAM is paged out
;;

pageOutDivMMC:
                push    af
                xor     a
                out     ($e3),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageVideo
;; Make sure the tilemap is available

pageVideo:
                page    2,10
                page    3,11
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; pageDebugger
;; Page in the debugger at MMU0
;;

pageDebugger:
                push    af
                call    pageOutDivMMC
                ld      a,(DbgCode)
                page    0,a
                ld      a,(Data)
                page    1,a
                ld      (DbgData),a
                pop     af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; farCall
;; Call a function in a different page
;;
;; Input:
;;      A = page
;;      HL = address ($e000-$ffff)
;;
;; Output:
;;      A = page
;;
;; Affected:
;;      All registers
;;

farCall:
                push    de
                ld      e,a
                rpage   7
                ld      d,a
                ld      (.pages),de     ; Store previous page and current page (D = old, E = new)

                ; Page in and call routine
                ld      a,e
                page    7,a
                pop     de              ; Restore DE
                callhl

                ; Return to caller
                push    de
                ld      de,(.pages)
                ld      a,d
                page    7,a
                ld      a,e
                pop     de
                ret

.pages          dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmAlloc
;; Allocate a page during assembly.
;;
;; Output:
;;      A = Page #
;;      CF = 1 if out of memory
;;

asmAlloc:
                jp      allocPage

;;----------------------------------------------------------------------------------------------------------------------
;; asmFree
;; Free a page previously allocated.  Uses OS if NexMode is 0.  Otherwise it's a no-op (deallocations are done at
;; the beginning of an assembly with pageReset).
;;
;; Input:
;;      A = Page #
;;
;; Affects:
;;      AF, AF'
;;

asmFree:
                jp      freePage

;;----------------------------------------------------------------------------------------------------------------------
;; calcHiPage
;; Calculate the highest page # on this machine.
;; NOTE: Requires the presence of NextZXOS and sysvars being paged in
;;

calcHiPage:
                ld      a,($5b69)
                add     a,a
                inc     a
                ld      (TopPage),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
