;;----------------------------------------------------------------------------------------------------------------------
;; Symbol management
;;
;; Symbols are stored in up to 8 pages (64K total).  Each symbol takes 32 bytes, which allows for 27 character symbols,
;; 1 byte for flags, 1 byte for null terminator, 1 byte for hash, and 2 bytes for address/value.  This allows for 2048 
;; symbols (256 symbols per page):
;;
;;                1         2         3
;;      01234567890123456789012345678901
;;      --------------------------------
;;      HCCCCCCCCCCCCCCCCCCCCCCCCCCC0FVV
;;
;;      H = Hash
;;      C = symbol name character
;;      0 = null terminator
;;      F = flags
;;      V = Value
;;
;; TODO: hash string to speed up search (hash is not currently supported)
;;----------------------------------------------------------------------------------------------------------------------

MAX_LEN_SYM     equ     27

SYM_VALUE       equ     30
SYM_FLAGS       equ     29
SYM_SIZE        equ     32

;;----------------------------------------------------------------------------------------------------------------------
;; symDone
;; Deallocate all memory used by symbols

symDone:
                ld      a,(NumSymPages)
                and     a
                ret     z
                ld      b,a
                ld      hl,SymPages
.l1:
                ldi     a,(hl)
                call    asmFree
                djnz    .l1
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symInit
;; Initialise the symbol state for a new assembly run.
;;

symInit:
                xor     a
                ld      bc,0
                ld      (NumSymPages),a
                ld      (NumSyms),bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symLookUp
;; Look up the value of a symbol
;;
;; Input:
;;      DE = symbol name
;;
;; Output:
;;      CF = 0 if symbol exist
;;      BC = value
;;      DE = address after null terminator
;;
;;      CF = 1 if symbol doesn't exist (DE and HL preserved)
;;
;; Affects:
;;      IX, Alternate registers
;;      MMU0 will be changed
;;

symLookUp:
                push    hl,de
                ex      de,hl           ; DE = symbol name
                call    symFind         ; DE = symbol address
                jr      nc,.error
                ld      a,SYM_VALUE
                add     de,a
                ex      de,hl
                ld      c,(hl)
                inc     l
                ld      b,(hl)          ; BC = value
                pop     hl              ; HL = symbol name
                call    strEnd
                inc     hl
                ex      de,hl           ; DE = end of symbol
                pop     hl
                and     a
                ret
.error:
                pop     de,hl
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symGet
;; Convert a symbol handle into an address and page it in to MMU0
;;
;; Input:
;;      DE = Symbol handle
;;
;; Output:
;;      DE = Address + corresponding MMU0 selected
;;
;; Affects:
;;      AF

symGet:
                push    hl
                ld      hl,SymPages
                ld      a,d
                add     hl,a
                ld      a,(hl)
                page    0,a             ; Page in page that contains the symbol

                ld      d,SYM_SIZE
                mul     de              ; DE = address of symbol
                pop     hl
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; symGetLength
;; Get the length of the symbol given between DE and HL and check to see it isn't too long
;;
;; Input:
;;      HL = symbol start
;;
;; Output:
;;      HL = symbol start
;;      BC = symbol length
;;      CF = 1 if symbol length OK
;;

symGetLength:
                ; Calculate the length of the symbol
                call    strlen          ; BC = length

                ; Check to see if the length isn't too long
                push    hl
                ld      hl,-(MAX_LEN_SYM+1)
                add     hl,bc
                ccf
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symAdd
;; Add a symbol to the symbol table
;;
;; Input:
;;      HL = symbol
;;
;; Output:
;;      if no error:
;;      DE = symbol handle (D = page #, E = symbol # in page)
;;      HL = advanced after null terminator
;;
;;      if error:
;;      error is displayed, output registers are undefined ; TODO: Ped: shouldn't this do something more stable, like DE = -1?
;;
;; Affects:
;;      AF, AF', BC
;;      MMU0 will be changed
;;

symAdd:
                call    symGetLength
                jr      nc,.too_long

                ; Do we need to allocate a new page for the symbol entries?
                ld      a,(NumSyms)     ; We have 256 symbols per 8K page, so if LSB is 0, we need new page
                and     a               ; Reached end of symbol page?  Need to add a new page
                jr      nz,.no_alloc

                ; Allocate a new page
                ld      a,(NumSymPages)
                cp      8
                jr      nc,.table_full
                ld      de,SymPages
                add     de,a            ; DE = address to store page # when allocated
                inc     a
                exa
                call    asmAlloc
                jr      c,.oom

                ld      (de),a          ; Store page #
                exa                     ; Get number of sym pages allocated
                ld      (NumSymPages),a

.no_alloc:
                ; Find the address to store the symbol
                ld      de,(NumSyms)    ; Get new symbol handle
                push    de              ; Store the symbol handle
                inc     de              ; Increment the number of symbols
                ld      (NumSyms),de
                dec     de
                call    symGet          ; DE = symbol address

                ; Copy the symbol name
                push    de              ; Store start of symbol table entry
                inc     bc              ; Include the null terminator
                ldir                    ; Copy symbol name into table
                pop     de
                ld      a,SYM_FLAGS
                add     de,a            ; DE = symbol info
                xor     a
                ldi     (de),a          ; Reset the flags
                ldi     (de),a
                ld      (de),a          ; Reset the value to 0
                pop     de              ; Restore the handle to return to caller
                ret

.too_long:
                call    errorPrint
                dz      "SYMBOL TOO LONG"
                ret

.table_full:
                call    errorPrint
                dz      "MAXIMUM NUMBER OF SYMBOLS REACHED"
                ret
.oom:
                call    errorPrint
                dz      "OUT OF MEMORY"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; symFind
;; Search for a symbol and return the symbol address with the symbol slot paged into MMU0
;;
;; Input:
;;      HL = symbol
;;
;; Output:
;;      BC = symbol handle
;;      DE = symbol address
;;      CF = 1 if found
;;
;; Affects:
;;      AF
;;
;; TODO: Encode symbol length in lex line, rather than use null terminator
;;

;;ASM 38E
symFind:
                ; Get the length and check to see if length is greater than maximum
                call    symGetLength
                ret     nc              ; If length is > 28 then we don't have this symbol in our table

                ld      bc,(NumSyms)
.next_sym:
                ld      a,b
                or      c
                ret     z               ; No more symbols, so not found

                dec     bc              ; [next] symbol handle to search
                ld      de,bc
                call    symGet
                ; HL = symbol to find
                ; BC = symbol handle
                ; DE = symbol address
                push    hl,de
.next_char:
                ldi     a,(de)
                cp      (hl)
                inc     hl
                jr      nz,.no_match
                or      a
                jr      nz,.next_char

                ; Found an exact match (including null terminators)
                pop     de,hl
                scf
                ret

.no_match:
                pop     de,hl
                jr      .next_sym




