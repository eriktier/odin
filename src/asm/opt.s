;;----------------------------------------------------------------------------------------------------------------------
;; OPTIONS
;;----------------------------------------------------------------------------------------------------------------------

;; Option table

OptTable:
                dz      "PAUSE"
                dw      optPause

                db      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmOPT
;; Handles the OPT command
;;
;; Input:
;;      DE = lexical line input (256-byte aligned)
;;

asmOPT:
                ld      a,(Pass)
                and     a
                jp      nz,asmSkipLine  ; On pass > 0, we don't process again

                ld      a,(de)
                cp      T_SYMBOL
                jr      nc,.no_error

                call    errorPrint
                dz      "INVALID OPT COMMAND"
                ret

.no_error:
                ld      hl,OptTable
                inc     e
                push    de              ; Store beginning of word to test against
.next_word:
                ; HL = Current word in opt table
                ; DE = input
                pop     de
                push    de              ; Restore DE to beginning of word to compare against

                ld      a,(hl)
                and     a
                jr      z,.not_found    ; End of opt table

.next_char:
                ld      a,(de)          ; Get next character in input
                inc     e
                and     a               ; Reached end?
                jr      z,.poss_match   ; Yes, check we reached the end of the opt table too
                cp      ':'
                jr      z,.poss_match   ; Word can be terminated by colon too

                res     5,a             ; Make sure it's uppercase
                cp      (hl)
                inc     hl
                jr      z,.next_char    ; Keep looping while we match

                ; We've found mismatch
                dec     hl
.l0:
                ld      a,(hl)
                inc     hl
                and     a
                jr      nz,.l0
                inc     hl
                inc     hl              ; Skip address
                jr      .next_word

.not_found:
                pop     de
                call    errorPrint
                dz      "UNKNOWN OPT COMMAND"
                ret

.poss_match:
                ld      a,(hl)
                and     a
                jr      nz,.l0          ; We didn't match
                inc     sp
                inc     sp              ; Drop DE in stack
                inc     hl
                ldhl
                jp      (hl)

;;----------------------------------------------------------------------------------------------------------------------
;; optTestEnd
;; Test that there are no parameters
;;
;; Output:
;;      CF = 1 if error
;;

optTestEnd:
                ld      a,(de)
                and     a
                ret     z
                call    errorPrint
                dz      "NO PARAMS EXPECTED AFTER OPT COMMAND"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; optPause
;; Set the debugger to wait for key before exiting
;;
;; Input:
;;      DE = lex input after label
;;

optPause:
                call    optTestEnd
                ret     c
                call    pageDebugger
                ld      a,1
                ld      (DebuggerPause),a
                ret

