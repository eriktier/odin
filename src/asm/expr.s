;;----------------------------------------------------------------------------------------------------------------------
;; Expression evaluator
;;----------------------------------------------------------------------------------------------------------------------

ET_OPEN         equ     '('
ET_CLOSE        equ     ')'
ET_PLUS         equ     '+'
ET_MINUS        equ     '-'
ET_OR           equ     '|'
ET_AND          equ     '&'
ET_XOR          equ     '^'
ET_MUL          equ     '*'
ET_DIV          equ     '/'
ET_MOD          equ     OP_MOD
ET_U_INVERT     equ     $80+'~'
ET_U_PLUS       equ     $80+'+'
ET_U_MINUS      equ     $80+'-'
ET_SHL          equ     OP_SHL
ET_SHR          equ     OP_SHR

;;----------------------------------------------------------------------------------------------------------------------
;; Operator table
;;

OpTable:
                db      ET_OPEN,        0
                db      ET_CLOSE,       0
                db      ET_PLUS,        2
                db      ET_MINUS,       2
                db      ET_OR,          5
                db      ET_AND,         4
                db      ET_XOR,         5
                db      ET_SHL,         3
                db      ET_SHR,         3
                db      ET_MUL,         1
                db      ET_DIV,         1
                db      ET_MOD,         1
                db      ET_U_INVERT,    0
                db      ET_U_PLUS,      0
                db      ET_U_MINUS,     0
                db      0

OpFuncTable:
                dw      exprPlus
                dw      exprMinus
                dw      exprOr
                dw      exprAnd
                dw      exprXor
                dw      exprShl
                dw      exprShr
                dw      exprMultiply
                dw      exprDivide
                dw      exprMod
                dw      exprUnaryInvert
                dw      exprUnaryPlus
                dw      exprUnaryMinus

;;----------------------------------------------------------------------------------------------------------------------
;; exprGet
;; Parse the input to calculate a 16-bit value
;;
;; Input:
;;      DE = start of expression (within 256-byte aligned buffer).
;;
;; Output:
;;      DE = end of expression
;;      HL = value of expression
;;      CF = 1 if error occurred
;;
;; Destroys:
;;      AF'
;;

exprGet:
                push    bc,ix,iy
                call    exprGet2
                pop     iy,ix,bc
                ret

                ; Register usage:
                ;
                ;       B = parentheses count
                ;       HL = start of output buffer (256-byte aligned)
exprGet2:
                ld      hl,ExprBuffer
                ld      b,l             ; L = 0
                ld      c,l

;;----------------------------------------------------------------------------------------------------------------------
;; State 0 of expression syntax

expr0:
                ld      a,(de)
                inc     e
                and     a                       ; Check EOL
                jp      z,exprSyntaxError

                cp      ET_OPEN
                jr      nz,.check_val

                ;
                ; Handle (
                ;
                inc     b
                call    exprAddOp
                ret     c
                jr      expr0
.check_val:
                ;
                ; Handle values
                ;
                cp      T_EXPR
                jr      nc,.not_0_value
                call    exprAddValue            ; Push value into input stream
                ret     c
                jr      expr1
.not_0_value:
                ;
                ; Handle unary operators
                ;
                bit     7,a
                jp      nz,exprSyntaxError      ; Not a unary operator!
                or      $80
                call    exprAddOp               ; Attempt to add the operator
                ret     c
                jr      expr2

;;----------------------------------------------------------------------------------------------------------------------
;; State 1 of expression syntax

expr1:
                ld      a,(de)
                and     a
                jp      z,exprProcess           ; Expression syntax check is complete

                cp      ET_CLOSE
                jr      nz,.not_1_cp

                ;
                ; Handle )
                ;
                exa
                ld      a,b
                and     a                       ; Is this a terminating )?
                jp      z,exprProcess
                dec     b
                exa
                call    exprAddOp
                ret     c
                inc     e
                jr      expr1
.not_1_cp:
                ;
                ; Handle ,
                ;
                cp      ','
                jp      z,exprProcess
                
                ;
                ; Handle binary operators
                ;
                call    exprAddOp               ; Attempt to add the operator
                ret     c
                inc     e
                jp      expr0

;;----------------------------------------------------------------------------------------------------------------------
;; State 2 of expression syntax

expr2:
                ld      a,(de)
                inc     e
                and     a
                jp      z,exprSyntaxError

                ;
                ; Handle (
                ;
                cp      ET_OPEN
                jr      nz,expr0.check_val
                inc     b
                call    exprAddOp
                ret     c
                jp      expr0

;;----------------------------------------------------------------------------------------------------------------------
;; exprCheckRoom
;; Check that there's room for adding an operator or value to the input stream.
;;
;; Input:
;;      HL = address of input stream before adding
;;      C = op stack position
;;
;; Output:
;;      CF = 1 if no room
;;      A = unchanged if no error
;;

exprCheckRoom:
                push    bc,hl,af
                ld      b,h
                ld      a,c
                and     a
                jr      nz,.not_0
                inc     b
.not_0:
                ; BC = Last address
                and     a               ; Clear CF
                add     hl,4            ; HL = new address after add
                sbc     hl,bc           ; ZF = 1 -> OK, otherwise CF = 1 -> OK
                jr      z,.ok
                jr      c,.ok
                call    exprProcess.push_error
                pop     af
                scf
                jr      .error
.ok:
                pop     af
                and     a
.error:
                pop     hl,bc
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprAddValue
;; Convert value tokens into an actual 24-bit value and store it in the buffer
;;
;; Input:
;;      A = token type
;;      DE = address of token params
;;      HL = current position in output buffer
;;
;; Output:
;;      DE = updated DE after token
;;      HL = updated HL in output buffer
;;      CF = 1 if error occurs
;;

exprAddValue:
                call    exprCheckRoom
                ret     c
                ld      (hl),0
                inc     l
                push    bc
                cp      T_SYMBOL
                jr      nz,.not_sym

                ;
                ; Handle symbol
                ;
                call    symLookUp               ; BC = value of symbol
                jr      c,.no_sym
.write:
                ld      (hl),c
                inc     l
                ld      (hl),b
                inc     l                       ; Add the value
                ld      (hl),0
                inc     l
                pop     bc
                and     a
                ret
.no_sym:
                ld      a,(Pass)
                and     a
                jr      z,.pass0
                pop     bc
                call    errorPrint
                dz      "UNKNOWN SYMBOL"
                ret
.pass0:
                ; Scan to the end of the symbol we didn't find
                ld      a,(de)
                inc     e
                and     a
                jr      nz,.pass0
                ld      b,a
                ld      c,a             ; BC = 0
                jr      .write

.not_sym:
                ;
                ; Handle values
                ;
                cp      T_VALUE
                jr      nz,.not_value
                ld      a,(de)
                inc     e
                ld      c,a
                ld      a,(de)
                inc     e
                ld      b,a
                jr      .write

.not_value:
                ;
                ; Handle strings
                ;
                cp      T_STRING
                jr      nz,.not_string
                ld      bc,0
.l0:
                ld      a,(de)
                inc     e
                and     a
                jr      z,.write
                ld      b,c
                ld      c,a
                jr      .l0

.not_string:
                ;
                ; Handle $
                ;
                ld      a,(LinePC)
                ld      c,a
                ld      a,(LinePC+1)
                ld      b,a
                jr      .write
                

;;----------------------------------------------------------------------------------------------------------------------
;; exprAddOp
;; Add an operator to the expression input buffer.
;;
;; Input:
;;      A = token
;;      HL = current position in output buffer
;;
;; Output:
;;      HL = updated HL in output buffer
;;      CF = 1 if token not an operator
;;

exprAddOp:
                call    exprCheckRoom
                ret     c
                push    bc,hl
                ld      hl,OpTable
                ld      c,a
                ld      b,0
.l0:
                inc     b
                ld      a,(hl)
                and     a               ; Reached end of table?
                jr      z,.error

                cp      c               ; Found the operator?
                inc     hl
                inc     hl
                jr      nz,.l0
                dec     b               ; B = index of operator

                ; Found the operator
                pop     hl
.send:
                ld      (hl),$fe
                inc     l
                ld      (hl),b
                inc     l
                xor     a
                ld      (hl),a
                inc     l
                ld      (hl),a
                inc     l
                pop     bc
                ret
.error:
                pop     hl,bc
                call    errorPrint
                dz      "INVALID OPERATOR"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprAddOpIndex
;; Same as exprAddOp, but the op index is already known
;;
;; Input:
;;      A = token
;;      HL = current position in output buffer
;;
;; Output:
;;      HL = updated HL in output buffer
;;      CF = 1 if token not an operator
;;

exprAddOpIndex:
                call    exprCheckRoom
                ret     c
                ld      (hl),$fe
                inc     l
                ld      (hl),a
                inc     l
                ld      (hl),0
                inc     l
                ld      (hl),0
                inc     l
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprProcess
;; The syntax checking process has completed.  Now we run the shunting yard algorithm on the input.
;;
;; Output:
;;      CF = 1 if error
;;

exprProcess:
                ld      (.final_de),de
                ld      a,b
                and     a
                jr      z,.paren_ok

                call    errorPrint
                dz      "UNBALANCED PARENTHESES"
                ret

.paren_ok:
                ; DE = Address to return (i.e. end of input stream)
                ; HL = top of input 
                ; Mark end of input stream
                call    exprCheckRoom
                ret     c
                ld      (hl),$ff                ; Mark end
                ld      bc,ExprBuffer+256       ; BC = top of op stack

                ld      hl,ExprBuffer           ; HL = output buffer
                push    hl
                pop     ix                      ; IX = input cursor
.l0:
                ld      a,(ix)                  ; Get type (value/op/end)
                inc     ixl
                inc     a
                jp      z,.end                  ; ZF = 1 if end ($ff)
                dec     a
                jp      z,.value                ; ZF = 1 if value ($00)

                ; A = operator token
                ld      a,(ix)                  ; Get operator index
                inc     ixl
                inc     ixl
                inc     ixl
                and     a
                jr      z,.open
                dec     a
                jr      z,.close
                inc     a

                ;
                ; Handle binary and unary operators
                ;
                ; Register allocation:
                ;
                ;       A' = op
                ;       IYL = input op level
                ;       BC = top of op stack at end of ExprBuffer
                ;       IX = Input cursor
                ;       HL = Output buffer
                ;
                push    af
                exa
                pop     af
                push    hl
                ld      hl,OpTable
                add     a,a
                add     hl,a                    ; HL = input operator info
                inc     hl
                ld      a,(hl)                  ; L = level of input op
                ld      iyl,a
                pop     hl
.loop:
                ; While op stack is not empty
                ld      a,c
                and     a
                jr      z,.end_loop

                ; While op stack top is not (
                ld      a,(bc)                  ; A = top op on op stack
                and     a
                jr      z,.end_loop

                ; Check operator levels
                ld      de,OpTable
                add     a,a
                add     de,a
                inc     de
                ld      a,(de)                  ; A = level of tos level
                cp      iyl

                ; If tos level < op level, CF = 1
                ; If tos level == op level, ZF = 1
                jr      c,.pop_op
                jr      nz,.end_loop
                ld      a,iyl
                and     a
                jr      z,.end_loop
.pop_op:
                ld      a,(bc)
                inc     bc
                call    exprAddOpIndex
                jr      c,.error
                jr      .loop
.error:
                pop     de
                ret
.end_loop:
                ; Push the op on the stack
                exa

.open:
                ; Handle (
                push    hl
                add     hl,4
                and     a
                sbc     hl,bc
                pop     hl
                jr      z,.push_error
                jr      nc,.push_error
                dec     bc
                ld      (bc),a
                jp      .l0
.push_error:
                call    errorPrint
                dz      "EXPRESSION TOO COMPLEX"
                ret

.close:
                ; Handle )
                ld      a,(bc)
                inc     bc              ; Pop value off
                and     a               ; Is value == '('?
                jp      z,.l0           ; Yes, next token
                call    exprAddOpIndex
                ret     c
                jr      .close

.value:
                call    exprCheckRoom
                ret     c
                ld      (hl),0
                inc     l
                push    .l0
                push    bc
                ld      bc,(ix+0)
                inc     ixl
                inc     ixl
                inc     ixl             ; DE = 16-bit value
                jp      exprAddValue.write

.end:
                ; Flush the op stack to the output stream
                ld      a,c
                and     a
                jr      z,.no_flush
.l1:
                call    exprCheckRoom
                ret     c
                ld      a,(bc)          ; Get operator
                ld      (hl),$fe
                inc     l
                ld      (hl),a
                inc     l
                xor     a
                ld      (hl),a
                inc     l
                ld      (hl),a
                inc     l
                inc     c
                jr      nz,.l1

                ; Terminate the op stack and clear the rest of the buffer
.no_flush:
                call    exprCheckRoom
                ret     c
                ld      (hl),$ff
                inc     l
                inc     l
                inc     l
                inc     l
                jr      z,.done_clear
                xor     a
.loop_clear:
                ld      (hl),a
                inc     l
                inc     l
                inc     l
                inc     l
                jr      nz,.loop_clear
.done_clear:

                ;
                ; Perform the calculations
                ;
                ; DE = input stream
                ; BC = top of numeric stack

                ex      de,hl                   ; DE = expression buffer
                ld      bc,de
.calc_loop:
                ld      a,(de)
                inc     e
                inc     a                       ; End of expression?
                jr      z,.calc_done
                dec     a                       ; Value
                jr      z,.calc_value

                ; Perform operation
                ld      hl,OpFuncTable
                ld      a,(de)
                sub     2                       ; Adjust index as () not included in function table
                add     a,a
                add     hl,a                    ; HL = address of func pointer
                ldhl                            ; HL = func pointer for operator
                push    de
                callhl                          ; Call operator
                pop     de
                ret     c
                add     de,3
                jr      .calc_loop

.calc_value:
                ;
                ; Push the value to the stack
                ;

                ld      a,(de)
                inc     e
                ld      l,a
                ld      a,(de)
                inc     e
                ld      h,a
                ld      a,(de)                  ; AHL = 24-bit value
                inc     e
                call    exprPushAHL
                ret     c
                jr      .calc_loop
.calc_done:
                ;
                ; Return the 16-bit value
                ;

                inc     c
                ld      a,(bc)
                inc     c
                ld      l,a
                ld      a,(bc)
                ld      h,a
                ld      de,(.final_de)
                and     a
                ret

.final_de:      dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; exprSyntaxError
;; Called when an error occurs in an expression
;;

exprSyntaxError:
                call    errorPrint
                dz      "SYNTAX ERROR IN EXPRESSION"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprError
;; Show a error stating an invalid expression

exprError:
                call    errorPrint
                dz      "INVALID EXPRESSION"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprIsNext
;; Check token in A to see if it's a valid expression starter.  If it isn't CF = 1
;;
;; Possible expression starting characters:
;;
;;      * Any value
;;      * Any symbol
;;      * Any unary operator: - ~
;;

exprIsNext:
                and     a
                scf
                ret     z               ; $00 -> not expression

                cp      T_EXPR
                ccf
                ret     nc              ; $01-$04 -> expression

                ; CF = 1
                ccf
                cp      '-'
                ret     z
                cp      '+'
                ret     z
                cp      '~'
                ret     z
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprPushAHL
;; Push a 24-bit value to the 32-bit stack
;;
;; Input:
;;      AHL = 24-bit value
;;      BC = top of stack
;;
;; Output:
;;      CF = 1 if out of stack
;;
;; Uses:
;;      A'
;;

exprPushAHL:
                exa
                dec     c
                dec     c
                dec     c
                dec     c

                ld      a,(bc)
                inc     a               ; Hit end of input stream?
                jp      z,exprProcess.push_error

                push    bc

                inc     c               ; Skip past type field
                ld      a,l
                ld      (bc),a
                inc     c
                ld      a,h
                ld      (bc),a
                inc     c
                exa
                ld      (bc),a
                pop     bc
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprPopDHL
;; Pop a 24-bit value from the 32-bit stack
;;
;; Input:
;;      BC = top of stack
;;
;; Output:
;;      DHL = 24-bit value
;;

exprPopDHL:
                inc     c

                ld      a,(bc)
                inc     c
                ld      l,a

                ld      a,(bc)
                inc     c
                ld      h,a

                ld      a,(bc)
                inc     c
                ld      d,a

                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Expression operator handlers
;;
;; Input:
;;      BC = top of stack
;;

exprPlus:
                push    bc
                pop     ix

                ld      a,(ix+1)
                add     a,(ix+5)
                ld      (ix+5),a

                ld      a,(ix+2)
                adc     a,(ix+6)
                ld      (ix+6),a

                ld      a,(ix+3)
                adc     a,(ix+7)
                ld      (ix+7),a

exprBinaryEnd:
                add     bc,4
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------

exprMinus:
                push    bc
                pop     ix

                ld      a,(ix+5)
                sub     (ix+1)
                ld      (ix+5),a

                ld      a,(ix+6)
                sbc     (ix+2)
                ld      (ix+6),a

                ld      a,(ix+7)
                sub     (ix+3)
                ld      (ix+7),a

                jp      exprBinaryEnd

;;----------------------------------------------------------------------------------------------------------------------

exprOr:
                push    bc
                pop     ix

                ld      a,(ix+1)
                or      (ix+5)
                ld      (ix+5),a

                ld      a,(ix+2)
                or      (ix+6)
                ld      (ix+6),a

                ld      a,(ix+3)
                or      (ix+7)
                ld      (ix+7),a

                jp      exprBinaryEnd

;;----------------------------------------------------------------------------------------------------------------------

exprAnd:
                push    bc
                pop     ix

                ld      a,(ix+1)
                and     (ix+5)
                ld      (ix+5),a

                ld      a,(ix+2)
                and     (ix+6)
                ld      (ix+6),a

                ld      a,(ix+3)
                and     (ix+7)
                ld      (ix+7),a

                jp      exprBinaryEnd

;;----------------------------------------------------------------------------------------------------------------------

exprXor:
                push    bc
                pop     ix

                ld      a,(ix+1)
                xor     (ix+5)
                ld      (ix+5),a

                ld      a,(ix+2)
                xor     (ix+6)
                ld      (ix+6),a

                ld      a,(ix+3)
                xor     (ix+7)
                ld      (ix+7),a

                jp      exprBinaryEnd

;;----------------------------------------------------------------------------------------------------------------------

exprShl:
                call    exprPopDHL              ; DHL = shift
                ld      a,d
                and     a
                jr      nz,.zero
                ld      a,h
                and     a
                jr      nz,.zero
                ld      a,l
                cp      24
                jr      nc,.zero

                ; Shift the value left
                ld      e,l
                call    exprPopDHL              ; DHL = value to shift
                call    exprTestPos
                ret     c
                ld      a,e
                and     a
                jr      z,.no_shift
.l0:
                sla     l
                rl      h
                rl      d
                dec     e
                jr      nz,.l0
.no_shift:
                ld      a,d
                call    exprPushAHL
                and     a
                ret

.zero:
                call    exprPopDHL

exprPushZero:
                ld      hl,0
                ld      a,h
                call    exprPushAHL
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; exprTestPos
;; Test to see if DHL is positive
;;
;; Input:
;;      DHL = value to test
;;
;; Output:
;;      CF = 1 if not positive
;;

exprTestPos:
                ld      a,d
                add     a,a
                ret     nc
                call    errorPrint
                dz      "POSITIVE VALUE EXPECTED"
                ret

;;----------------------------------------------------------------------------------------------------------------------

exprShr:
                call    exprPopDHL              ; DHL = shift
                ld      a,d
                and     a
                jr      nz,.zero
                ld      a,h
                and     a
                jr      nz,.zero
                ld      a,l
                cp      24
                jr      nc,.zero

                ; Shift the value left
                ld      e,l
                call    exprPopDHL              ; DHL = value to shift
                call    exprTestPos
                ret     c
                ld      a,e
                and     a
                jr      z,.no_shift
.l0:
                sra     d
                rr      h
                rr      l
                dec     e
                jr      nz,.l0
.no_shift:
                ld      a,d
                call    exprPushAHL
                and     a
                ret

.zero:
                call    exprPopDHL
                jr      exprPushZero

;;----------------------------------------------------------------------------------------------------------------------

exprMultiply:
                push    bc
                push    bc
                pop     ix

                ; First value
                ld      d,0
                ld      e,(ix+3)
                ld      h,(ix+2)
                ld      l,(ix+1)
                exx

                ; Second value
                ld      d,0
                ld      e,(ix+7)
                ld      h,(ix+6)
                ld      l,(ix+5)
                exx

                call    mul_32_32_32            ; DEHL = result
                pop     bc
                ld      a,c
                add     a,8
                ld      c,a
                ld      a,e
                jp      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

divmod:
                push    bc
                pop     ix

                ; First value (divisor)
                ld      d,0
                ld      e,(ix+3)
                ld      h,(ix+2)
                ld      l,(ix+1)
                exx

                ; Second value (dividend)
                ld      d,0
                ld      e,(ix+7)
                ld      h,(ix+6)
                ld      l,(ix+5)
                exx

                jp      divs_32_32_32

exprDivide:
                push    bc
                call    divmod
                pop     bc
                ld      a,c
                add     a,8
                ld      c,a
                ld      a,e
                jp      exprPushAHL

exprMod:
                push    bc
                call    divmod
                exx
                pop     bc
                ld      a,c
                add     a,8
                ld      c,a
                ld      a,e
                jp      exprPushAHL

;;----------------------------------------------------------------------------------------------------------------------

exprInvert:
                ld      a,l
                cpl
                ld      l,a
                ld      a,h
                cpl
                ld      h,a
                ld      a,d
                cpl
                ret

exprUnaryInvert:
                call    exprPopDHL
                call    exprInvert
                call    exprPushAHL
                ret

exprUnaryPlus:
                ret

exprUnaryMinus:
                call    exprPopDHL
                call    exprInvert              ; AHL = inverted value
                ld      d,a
                xor     a
                inc     l
                adc     a,h
                ld      a,0
                adc     a,d
                call    exprPushAHL
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------


