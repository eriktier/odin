;;----------------------------------------------------------------------------------------------------------------------
;; Emitting bytes
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; emitWord
;; Emits a 16-bit word, little endian.
;;
;; Input:
;;      HL = word to emit
;;
;; Side effects:
;;      Advances PC by 2
;;
;; Output:
;;      B = high byte written
;;      CF = 1 of error occurs
;;

emitWord:
                ld      b,l
                call    emitB
                ret     c
                ld      a,h

;;----------------------------------------------------------------------------------------------------------------------
;; emitByte
;; Emits a single byte
;;
;; Input:
;;      A = byte to emit
;;
;; Side effects:
;;      Advances PC
;;
;; Output:
;;      B = byte written
;;      CF = 1 if error occurs
;;

emitByte:
                ld      b,a

;;----------------------------------------------------------------------------------------------------------------------
;; emitB
;; Emits a single byte in the B register
;;
;; Input:
;;      B = byte to emit
;;
;; Side effects:
;;      Advances PC
;;
;; Output:
;;      B = byte written
;;      CF = 1 if error occurs
;;
;; Unaffected:
;;      C, Alternate registers, IX, IY, DE, HL
;;

emitB:
                push    de,hl

                ; TODO: Check page border rather than calculating MMU state each byte

        IF DEBUG_ASM
                push    af,bc,de,hl
                call    pageVideo
                ld      a,b
                call    printHexByte
                call    printSpace
                call    asmPageDoc
                pop     hl,de,bc,af
        ENDIF

                ; If we're in pass 0, don't write any bytes
                ld      a,(Pass)
                and     a
                jr      z,.update_pc

                ; Write the byte in
                ld      hl,(WritePC)
                ld      de,MMUState
                ld      a,h
                rlca
                rlca
                rlca
                and     7
                add     de,a
                ld      a,(de)          ; Get the page this byte is in
                cp      $ff
                jr      z,.rom_error
                page    6,a

                ld      a,h
                and     $1f
                or      $c0
                ld      h,a             ; Convert HL to real address

                ld      (hl),b          ; Write byte

                ld      a,(AsmSharedPage)
                page    6,a             ; Restore page for shared code

.update_pc:
                ; CF = 0 from both branches
                ld      hl,(PC)
                inc     hl
                ld      (PC),hl
                ld      hl,(WritePC)
                inc     hl
                ld      (WritePC),hl
                pop     hl,de
                xor     a               ; Ensure ZF = 1 for some callers
                ret

.rom_error:
                call    errorPrint
                dz      "ATTEMPT TO ASSEMBLE TO INVALID MEMORY"
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; emitOpByte
;; Emit a single opcode value and a byte
;;
;; Input:
;;      A = opcode
;;      L = byte
;;
;; Output:
;;      CF = 1 if error occurs
;;

emitOpByte:
                call    emitByte
                ret     c
                ld      b,l
                jp      emitB

;;----------------------------------------------------------------------------------------------------------------------
;; emitOpAddr
;; Emit a single opcode value and an address
;;
;; Input:
;;      A = opcode
;;      HL = address
;;
;; Output:
;;      CF = 1 if error occurs
;;

emitOpAddr:
                call    emitByte
                ret     c
                jp      emitWord

;;----------------------------------------------------------------------------------------------------------------------
;; emitCBOp
;; Emit CB followed by a byte in A
;;

emitCBOp:
                ld      c,a
                ld      b,$cb
                call    emitB
                ld      b,c
                jp      emitB

;;----------------------------------------------------------------------------------------------------------------------
;; emitEDOp
;; Emit CB followed by a byte in A
;;

emitEDOp:
                ld      c,a
emitEDOpC:
                ld      b,$ed
                call    emitB
                ld      b,c
                jp      emitB

