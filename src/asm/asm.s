;;----------------------------------------------------------------------------------------------------------------------
;; Assembler
;;----------------------------------------------------------------------------------------------------------------------

; TODO: Merge exprIsNext into exprGet?
; TODO: Make exprGet8?

;;----------------------------------------------------------------------------------------------------------------------
;; Global variables
;;----------------------------------------------------------------------------------------------------------------------

Pass            db      0               ; Current pass number (0/1)
PC              dw      0               ; Current virtual PC
LinePC          dw      0               ; PC when line is starting to be assembled
WritePC         dw      0               ; Current position to write bytes to in the virtual memory
AsmSharedPage   db      0               ; Copy of Shared page
ReadOpcode      db      0               ; Used to determine which table to detokenise from in spurious errors

IndexDisp       db      0               ; Displacement found in (IX/Y+nn)

;;----------------------------------------------------------------------------------------------------------------------
;; AsmContext
;; This structure keeps track of the state of a in-memory file being assembled.  The assembler maintains a stack of
;; these and AsmFile is a pointer to the top element of the stack.
;;

                STRUCT AsmContext

Line            dw      0               ; Address of current line in document
LineNum         dw      0               ; Current line number in document being assembled
Pages           ds      6               ; 6 Pages used for the document (0 = not used)
FileName        dw      0               ; Address in AsmFNamePage to find filename.
Flags           db      0               ; Bit 0 = 1 if loaded from file

                ENDS

kNumAsmDocs     equ     high 8192       ; Each filename is 256 chars max

AsmFNamePage    db      0               ; Page used to store filenames during assembly.
AsmStack        ds      AsmContext * kNumAsmDocs
AsmEndStack     equ     $
AsmFName        dw      0               ; Points to TOS for filenames.
AsmFirstDoc     db      0               ; Index of first document to assemble.

        IF DEBUG_ASM
LineCounter     dw      0
BrkLine         dw      $ffff

CLC             macro
                ei
                call    checkLineCount
                di
                endm
        ENDIF

;;----------------------------------------------------------------------------------------------------------------------
;; Assembler main loop
;;
;; Input:
;;      B = document index to assemble (usually 0)
;;
;; Ouput:
;;      CF = 1 if error occurs
;;      HL = Address of current line if error occurs
;;


asmMain:
                di
                ld      a,b
                ld      (AsmFirstDoc),a

                ; Create a page to store the filenames
                call    asmAlloc
                ld      (AsmFNamePage),a
                jp      c,asmOOM

                call    asmDone
                call    asmInit
                call    symDone
                call    symInit
                call    pageVideo
                call    cursorHide

.next_pass:
        IF DEBUG_ASM
                call    resetLineCount
                ld      hl,0
                ld      (LineCounter),hl
                call    pageVideo
                call    print
                dz      "*** PASS "
                ld      a,(Pass)
                call    printDigit
                ld      a,C_ENTER
                call    printChar
                CLC
                call    asmPageDoc
        ENDIF
                call    asmStartPass            ; DE = Beginning of document
.loop:
                ; Lexically analyser current line
                ld      a,1
                ld      (ReadOpcode),a          ; Reset the read opcode flag (1 = no read opcode)

        IF DEBUG_ASM
                push    bc,de,hl
                call    pageVideo
                call    print
                dz      "--- Line "
                ld      e,0
                ld      hl,(iy+AsmContext.LineNum)
                call    unpackD24
                call    printBCD5
                call    print
                dz      " ("
                call    asmDocName
                ex      de,hl
                call    printHL
                ld      a,c
                page    5,a
                call    print
                dz      ")",C_ENTER
                CLC
                call    asmPageDoc
                pop     hl,de,bc
        ENDIF

                call    asmPageDoc
                ld      hl,(PC)
                ld      (LinePC),hl
                ld      de,(iy+AsmContext.Line)

                ; Check to see if reached end of document
                ld      a,(de)
                inc     a               ; = $ff?
                jp      z,.eodoc        ; Yes, end of document

        IF DEBUG_ASM
                push    af,bc,de,hl
                push    de
                call    pageVideo
                ld      a,kInkSuccess
                call    setColour
                ld      e,0
                ld      hl,(LineCounter)
                call    unpackD24
                call    printBCD5
                ld      a,kInkNormal
                call    setColour
                call    print
                dz      " "
                pop     de
                call    asmPageDoc
                ex      de,hl
                ld      de,AsmBuffer
                call    detokenise
                ld      hl,AsmBuffer
                call    pageVideo
                call    printHL
                ld      a,C_ENTER
                call    printChar
                CLC
                call    asmPageDoc
                pop     hl,de,bc,af
        ENDIF

                call    lex             ; HL = Current line lexical tokens, DE = next source line
                ld      (iy+AsmContext.Line),de

        IF DEBUG_LEX
                push    af
                call    pageVideo
                ld      a,C_ENTER
                call    printChar
                CLC
                call    asmPageDoc
                pop     af
        ENDIF
                jr      c,.error

        IF DEBUG_ASM
                push    de,hl
                ld      hl,(LineCounter)
                ld      de,(BrkLine)
                call    compare16
                jr      nz,.no_break
                break
.no_break:
                inc     hl
                ld      (LineCounter),hl
                pop     hl,de
        ENDIF

                call    asmLine         ; Assemble line at HL

        IF DEBUG_ASM
                push    af,bc,de,hl
                call    pageVideo
                ld      a,C_ENTER
                call    printChar
                CLC
                call    asmPageDoc
                pop     hl,de,bc,af
        ENDIF

                jr      c,.error

                ld      de,(iy+AsmContext.LineNum)
                inc     de
                ld      (iy+AsmContext.LineNum),de
                jp      .loop

.eodoc:
                ; Pop the context, and if no more to pop, go to next pass
                call    asmPopDoc
                push    iy
                pop     hl
                ld      de,AsmStack-AsmContext
                call    compare16
                jp      nz,.loop

                ld      a,(Pass)
                inc     a
                cp      2               ; Done 2 passes?
                jr      z,.eoasm
                ld      (Pass),a
                jp      .next_pass

.error:
                ; Output file name when error occurs
                ld      a,kInkNormal
                call    setColour
                call    pageVideo
                call    asmDocName
                ex      de,hl
                call    printHL
                call    print
                dz      ": "
                ld      a,kInkNormal
                call    setColour

                ld      a,kInkError
                call    setColour
                call    print
                dz      "ERROR IN LINE "
                ld      hl,(iy+AsmContext.LineNum)
                inc     hl
                ld      e,0
                call    unpackD24
                call    printBCD5
                call    print
                dz      C_ENTER
                ld      a,kInkNormal
                call    setColour

                push    hl
                call    asmClearDocs
                scf
                pop     hl
.eoasm:
                call    pageVideo
                call    cursorShow

                ld      a,(AsmFNamePage)
                call    asmFree
                jp      resetKeys               ; Reset keyboard buffer and enable interrupts

;;----------------------------------------------------------------------------------------------------------------------
;; asmOOM
;; Display out of memory error
;;
;; Output:
;;      CF = 1
;;

asmOOM:
                call    errorPrint
                dz      "OUT OF MEMORY FOR ASSEMBLY"
                jp      asmMain.error

;;----------------------------------------------------------------------------------------------------------------------
;; asmDone
;; Deallocate all memory used by the last assembly

asmDone:
                ; Deallocate initial pages
                ld      hl,CodePages
                ld      b,4
.l1:
                ldi     a,(hl)
                and     a
                jr      nz,.free
                djnz    .l1
                jr      .done
.free
                call    asmFree
                djnz    .l1

                ; Reset symbol table
.done
                jp      symDone

;;----------------------------------------------------------------------------------------------------------------------
;; asmClearDocs
;; Clear the assembler context stack ready for new assembly and new pass.
;;

asmClearDocs:
                call    asmPopDoc
                ret     c
                jr      asmClearDocs

;;----------------------------------------------------------------------------------------------------------------------
;; asmAllocDoc
;; Internal function to allocate a document slot and copy filename
;;
;; Input:
;;      HL' = filename pointer
;;
;; Output:
;;      CF = 1 if overflow occurs.

asmAllocDoc:
                ld      a,(Pass)
                and     a
                jr      z,.pass0
                call    pageVideo
                call    print
                dz      "Assembling: "
                exx
                push    hl
                call    printHL
                pop     hl
                exx
                ld      a,C_ENTER
                call    printChar
.pass0:

                ; Check to see we're not full
                push    iy
                pop     hl
                ld      de,AsmContext
                add     hl,de
                ld      de,AsmEndStack
                call    compare16
                jr      z,.overflow

                ; We're good!  Update IY and document
                push    hl
                pop     iy

                ; Assume some defaults (we leave the pages and flags alone)
                ld      de,kDocStart
                ld      (iy+AsmContext.Line),de                 ; AsmContext.Line
                ld      (iy+AsmContext.LineNum+0),d
                ld      (iy+AsmContext.LineNum+1),d             ; AsmContext.LineNum

                ; Copy the filename into the filename stack
                exx
                push    hl
                exx
                pop     hl
                call    asmDocName      ; DE = address of filename buffer, C = old MMU 5 page
                inc     d               ; Point to next filename buffer
                ld      (AsmFName),de
                ld      (iy+AsmContext.FileName),de
                push    bc
                ld      bc,256
                call    memcpy
                pop     bc

                ; Restore MMU 5
                ld      a,c
                page    5,a

                and     a
                ret

.overflow:
                call    errorPrint
                dz      "INCLUDE DEPTH TOO LARGE"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmPushDoc
;; Push a file or memory-based document (by name) on the assembler stack to start building code
;;
;; Input:
;;      FileName = filename to look for
;;
;; Output:
;;      CF = 1 if stack overflow or loading error
;;
;; Destroys:
;;      BC, DE, HL
;;

asmPushDoc:
                ; Check validity of filename
                ld      hl,FileName
                ld      a,(hl)
                and     a
                jr      nz,.not_empty
                call    errorPrint
                dz      "INVALID FILENAME"
                ret

.not_empty:
                ; We now search to see if we already have this file loaded.  If so, use asmPushMemDoc.
                call    docFind                 ; A = index
                jp      nc,asmPushMemDoc        ; File in memory

                ; Check to see we're not full
                exx
                ld      hl,FileName
                exx                             ; HL' = address of document name
                call    asmAllocDoc
                ret     c

                ; Load the file from disk
                push    iy
                pop     hl                      ; HL = new document
                add     hl,AsmContext.Pages     ; HL = page array to write to on load
                call    docLoadPages
                jr      nc,.file_loaded

                call    errorPrint
                dz      "FAILED TO LOAD INCLUDE FILE"

                ld      de,-AsmContext
                add     iy,de                   ; Undo the allocation
                ret

.file_loaded:
                ; Copy the pages 
                ld      a,1
                ld      (iy+AsmContext.Flags),a         ; AsmContext.Flags (1 = loaded)

                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmPushMemDoc
;; Push a memory-based document on the assembler stack to start building code
;;
;; Input:
;;      A = document index
;;
;; Output:
;;      CF = 1 if stack overflow
;;
;; Destroys:
;;      BC, DE, HL
;;

asmPushMemDoc:
                push    af

                ; Get the document name
                call    docName                 ; HL = document name, B = old page
                exx                             ; HL' = document name
                call    asmAllocDoc
                ret     c

                xor     a
                ld      (iy+AsmContext.Flags),a         ; Clear flags (document from memory)
                pop     af
                call    docInfo                         ; HL = document information

                ; Copy pages information
                add     hl,Document.Pages
                push    iy
                pop     de
                add     de,AsmContext.Pages
                ld      bc,6
                ldir

                ; B = MMU 4 old page, C = MMU 5 old page.  Page them back
                ld      a,b
                page    4,a

                and     a                                       ; Ensure CF = 0
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmPageDoc
;; Assembler's version of paging in a document.  This is the same as pageDoc, except it doesn't act on MainDoc but
;; rather the document pages in the current context (pointed to by IY).
;;

asmPageDoc:
                push    af,bc,hl
                xor     a
                out     ($e3),a
                bchilo  6,REG_MMU0
                push    iy
                pop     hl
                add     hl,AsmContext.Pages
.l1:
                ld      a,c
                ld      (.page),a
                inc     c
                ldi     a,(hl)
                page    0,a
.page           equ     $-1
                djnz    .l1
                pop     hl,bc,af
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmDocName
;; Get a pointer to the current assembled document name.  This will page in the filenames page at MMU 5
;;
;; Output:
;;      DE = Address (in MMU 5) of filename
;;      C = old page at MMU 5
;;

asmDocName:
                rpage   5
                ld      c,a
                ld      a,(AsmFNamePage)
                page    5,a
                ld      de,(AsmFName)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmPopDoc
;; Pop a document off the stack
;;
;; Output:
;;      DE = source address
;;      CF = 1 if cannot pop any more docs
;;      

asmPopDoc:
                ; Check to see if we can pop, if not error out
                ld      de,AsmStack-AsmContext
                push    iy
                pop     hl
                call    compare16
                scf
                ret     z

                ; If file was loaded, clear the pages
                ld      a,(iy+AsmContext.Flags)
                and     a
                jr      z,.not_file

                ; Free the pages used
                push    iy
                pop     hl
                add     hl,AsmContext.Pages
                ld      b,6
.l0:
                ld      a,(hl)
                and     a
                jr      z,.not_file
                call    asmFree
                inc     hl
                djnz    .l0

.not_file:
                ld      de,-AsmContext
                add     iy,de

                ; Pop off filename
                ld      hl,(AsmFName)
                dec     h
                ld      (AsmFName),hl

                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmInit
;; Set up variables for a new assembler run
;;

asmInit:
                xor     a
                ld      (Pass),a

                ; Initialise assembler context and filename stack
                ld      iy,AsmStack - AsmContext
                ld      hl,addrMMU5 - 256
                ld      (AsmFName),hl

                ld      de,CodePages
                ld      b,4
.l1:
                call    asmAlloc
                ldi     (de),a
                djnz    .l1

                ld      hl,$8000
                ld      (StartAddress),hl

                ld      a,(SharedCode)
                ld      (AsmSharedPage),a

                ; Initialise debugger
                call    pageDebugger
                xor     a
                ld      (DebuggerPause),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmStartPass
;; Reset assembler state for new pass.
;;

asmStartPass:
                ; Organise pages for initial assembly - $ff = do not assemble here!
                ;
                ; 0 - $ff
                ; 1 - $ff
                ; 2 - $ff
                ; 3 - $ff
                ; 4 - PAGE 0
                ; 5 - PAGE 1
                ; 6 - PAGE 2
                ; 7 - PAGE 3
                ld      a,$ff
                ld      de,MMUState
                ldi     (de),a
                ldi     (de),a
                ldi     (de),a
                ldi     (de),a
                ld      hl,CodePages
                ld      bc,4
                ldir

                ; Initialise context stack
                call    asmClearDocs
                ld      a,(AsmFirstDoc)
                call    asmPushMemDoc

                ; Initialise PC
                ld      hl,$8000
                ld      (PC),hl
                ld      (WritePC),hl

                ; Intialise line number
                ld      de,kDocStart    ; Start of document
                xor     a
                ld      (iy+AsmContext.LineNum),a
                ld      (iy+AsmContext.LineNum+1),a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmLine
;; Assemble a single line
;;
;; Input:
;;      HL = Tokenised line of assembly
;;
;; Output:
;;      CF = 1 if error occurred
;;

asmLine:
                ld      de,$ffff
                ld      (SymHandle),de

                ld      a,(hl)
                and     a
                ret     z               ; Do nothing if blank line

                cp      T_SYMBOL
                jr      nz,.no_symbol
                inc     l

                ld      a,(Pass)
                and     a
                jr      nz,.skipSym     ; Only process labels on Pass 0

                ; Check to see if we've used this symbol already
                call    symFind
                jr      c,.duplicate_sym

                ; Add an entry to the symbol table

                call    symAdd          ; DE = symbol handle
                ld      (SymHandle),de
                call    symGet          ; Page in symbol table and get address
                ld      a,SYM_VALUE
                add     de,a
                ld      a,(PC)
                ldi     (de),a
                ld      a,(PC+1)
                ld      (de),a          ; Write PC as the value of the symbol
                jr      .no_symbol

.duplicate_sym:
                call    errorPrint
                dz      "DUPLICATE SYMBOL FOUND"
                ret

.skipSym:
                ; Read the symbol and find it, it should exists on Pass 1
                call    symFind
                ld      (SymHandle),bc
                call    strEnd
                inc     l

.no_symbol:
                ; Check EOL
                ld      a,(hl)
                and     a
                ret     z
                inc     l

                ; Check for token
                cp      $80
                jr      c,asmUnknownToken

                ; Handle the token
                push    af
                xor     a
                ld      (ReadOpcode),a
                pop     af

                call    asmHandleToken
                ret     c
                jp      asmTestEnd

;;----------------------------------------------------------------------------------------------------------------------
;; asmHandleToken
;; Handle a directive or instruction token and jump to its routine
;;
;; Input:
;;      A = token
;;      HL = operands in line
;;
;; Parameters for Handlers:
;;      A = first parameter token
;;      DE = address of first parameter
;;      B = opcode base (used sometimes to generate bit patterns for opcodes)
;;
;;      HL & C are unused.
;;

asmHandleToken:
                ex      de,hl                   ; DE = operands stream
                ld      hl,OpcodeBaseTable
                sub     $80                     ; Move opcode into range $00-$7f
                add     hl,a
                ld      b,(hl)                  ; B = opcode base
                ld      hl,AsmTokenTable
                add     a,a
                add     hl,a
                ldhl
                ld      a,(de)                  ; Load A with first token after opcode
                jp      (hl)

SymHandle       dw      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmUnknownToken
;;

asmUnknownToken:
                call    errorPrint
                dz      "UNKNOWN INSTRUCTION/DIRECTIVE"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmCheck
;; Check that a particular character in A is next in the stream, then consume it.
;;
;; Input:
;;      C = character to check against
;;      DE = token stream
;;
;; Output:
;;      A = next token stream after checked character
;;      DE = pointer to next token in stream after checked character
;;      CF = 1 if not matched
;;

asmCheck:
                ld      a,(de)
                cp      c
                scf
                ret     nz
                inc     e
                ld      a,(de)
                and     a
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmCheckSeq
;; Check that a series of tokens are in the stream.  Token are placed after a call using DZ.
;;
;; Input:
;;      DE = token stream
;;
;; Output:
;;      DE = pointer to next token in stream after sequence
;;      A = first token after sequence
;;      CF = 1 if no match, DE and A meaningless.
;;

asmCheckSeq:
                ex      (sp),hl                 ; HL = pointer to sequence
                ex      de,hl
.l0:
                ld      a,(de)                  ; A = next character in sequence
                and     a
                jr      z,.end

                cp      (hl)
                scf
                jr      nz,.end
                inc     l
                inc     de
                jr      .l0
.end:
                ex      de,hl
                ex      (sp),hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmAcceptComma
;; Consume a comma in the token stream.
;;
;; Input:
;;      DE = token stream
;;
;; Output:
;;      A = next non-comma token
;;      DE = token stream past comma
;;
;; Uses:
;;      C
;;

asmAcceptComma:
                ld      c,','
                jp      asmCheck

;;----------------------------------------------------------------------------------------------------------------------
;; asmHL_IX_IY
;; Tries to match either HL, IX or IY for the source parameter.  Will write the prefix if index registers are used.
;;
;; Input:
;;      A = token
;;
;; Output:
;;      ZF = 1 if match found
;;      CF = 1 if indexed
;;

asmHL_IX_IY:
                cp      OP_HL
                ret     z

                ; Check for IX
                cp      OP_IX
                jr      nz,.not_ix
                push    bc
                ld      b,$dd
                call    emitB
                pop     bc
                scf
                ret
.not_ix:
                ; Check for IY
                cp      OP_IY
                ret     nz
                push    bc
                ld      b,$fd
                call    emitB
                pop     bc
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmH_IXH_IYH
;; Tries to match either H, IXH or IYH for the source parameter.  Will write the prefix if index registers are used.
;;
;; Input:
;;      A = token
;;
;; Output:
;;      ZF = 1 if match found
;;

asmH_IXH_IYH:
                cp      OP_H
                ret     z

                ; Check for IXH
                cp      OP_IXH
                jr      nz,.not_ix
                push    bc
                ld      b,$dd
                call    emitB
                pop     bc
                scf
                ret
.not_ix:
                ; Check for IYH
                cp      OP_IYH
                ret     nz
                push    bc
                ld      b,$fd
                call    emitB
                pop     bc
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmL_IXL_IYL
;; Tries to match either L, IXL or IYL for the source parameter.  Will write the prefix if index registers are used.
;;
;; Input:
;;      A = token
;;
;; Output:
;;      ZF = 1 if match found
;;

asmL_IXL_IYL:
                cp      OP_L
                ret     z

                ; Check for IXL
                cp      OP_IXL
                jr      nz,.not_ix
                push    bc
                ld      b,$dd
                call    emitB
                pop     bc
                scf
                ret
.not_ix:
                ; Check for IYL
                cp      OP_IYL
                ret     nz
                push    bc
                ld      b,$fd
                call    emitB
                pop     bc
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asm16bitReg_0
;; Tries to match a 16 bit register.  H holds the token for the 4th register that can be matched.
;; This is either SP or AF.
;;
;; Input:
;;      A = token
;;      H = 4th register to match
;;      DE = address of token
;;
;; Output:
;;      C = index
;;      ZF = 1 if match found
;;      CF = 1 if indexed
;;      DE = address past token
;;

asm16bitReg_0:
                inc     e
                ld      c,0
                cp      OP_BC
                ret     z

                inc     c
                cp      OP_DE
                ret     z

                inc     c
                call    asmHL_IX_IY
                ret     z

                inc     c
                cp      h
                ret     z

                dec     e
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asm16bitReg_1
;; Tries to match a 16-bit register.  H holds the token for the 3rd register that can be matched.
;; This is either HL, IX or IY.
;;
;; Input:
;;      A = token
;;      H = 3rd register to match
;;      DE = address of token
;;
;; Output:
;;      C = index
;;      ZF = 1 if match found
;;      ZF = 0 if no match found, DE unchanged
;;      DE = address past token
;;

asm16bitReg_1_NoIndex:
                ld      h,OP_HL

asm16bitReg_1:
                inc     e
                ld      c,0
                cp      OP_BC
                ret     z

                inc     c
                cp      OP_DE
                ret     z

                inc     c
                cp      h
                ret     z

                inc     c
                cp      OP_SP
                ret     z

                dec     e
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asm8Bit
;; Tries to match an 8-bit register.
;;
;; Input:
;;      A = token
;;      DE = points to address
;;
;; Output:
;;      C = register index:
;;              0: B
;;              1: C
;;              2: D
;;              3: E
;;              4: H, IXH, IYH
;;              5: L, IXL, IYL
;;              6: (HL), (IX+disp), (IY+disp)
;;              7: A
;;      L = displacement if any
;;      H = index token if found
;;      ZF = 1 if match occurred
;;      CF = 1 if indexed, and hence L is valid (only when ZF = 1)
;;      CF = 1 if error occurred (only when ZF = 0)
;;      DE = points to next token
;;

asm8Bit:
                inc     e

                ld      c,0
                cp      OP_B
                ret     z

                inc     c
                cp      OP_C
                ret     z

                inc     c
                cp      OP_D
                ret     z

                inc     c
                cp      OP_E
                ret     z

                inc     c
                ld      h,a
                call    asmH_IXH_IYH
                ret     z

                inc     c
                ld      h,a
                call    asmL_IXL_IYL
                ret     z

                inc     c
                cp      '('
                jr      z,.parse_hl

                inc     c
                cp      OP_A
                ret     z

                dec     e
                scf
                ccf
                ret     ; No match so ZF = 0, CF = 0

.parse_hl:
                push    de                      ; Store original just in case of error
                ld      a,(de)
                ld      h,a
                call    asmHL_IX_IY             ; ZF = 0 on error, CF = 1 if indexed
                jr      nz,.error
                jr      c,.indexed
                inc     e
.match_close:
                ld      a,(de)
                inc     e                       ; Consume possible )
                cp      ')'
                jr      z,.good
.error:
                pop     de                      ; Restore token pointer
                scf                             ; Set error
                ret                             ; ZF = 0, CF = 1
.indexed_good:
                scf
.good:
                inc     sp
                inc     sp                      ; Drop saved DE
                ret                             ; ZF = 1, CF = 0/1

.indexed:
                ; Is it (ix+nn)?
                inc     e                       ; Consume IX/IY
                ld      a,(de)
                cp      '+'
                jr      z,.calc
                cp      '-'
                jr      z,.calc
.match_index_close:
                ld      a,(de)
                inc     e
                cp      ')'
                jr      z,.indexed_good
                jr      .error
.calc:
                ld      a,h
                exx
                ld      h,a
                exx                             ; H' = indexed token
                call    exprGet                 ; HL = displacement byte
                jr      c,.error
                call    asmValidDisp
                jr      c,.error
                exx
                ld      a,h
                exx
                ld      h,a                     ; H = token byte
                jr      .match_index_close

;;----------------------------------------------------------------------------------------------------------------------
;; asmCC
;; Get the condition code if any.
;;
;; Input:
;;      A = token
;;
;; Output:
;;      A = bit pattern representing code
;;      DE = address after token
;;      CF = 1 if no match found and DE is unchanged

asmCC:
                push    bc,hl
                ld      hl,CCTable
                ld      b,8
.l0:
                cp      (hl)
                jr      z,.found
                inc     hl
                djnz    .l0

                ; Not found
                scf
                jr      .end
.found:
                ; Found!
                ld      a,b
                sub     8
                neg
                swap
                srl     a
                inc     e
.end
                pop     hl,bc
                ret

CCTable:        db      $93,$99,$91,$81,$96,$95,$94,$90

;;----------------------------------------------------------------------------------------------------------------------
;; asmEQU
;; Handle EQU directive
;;

asmEQU:
                ; Check to see if we defined a label on this very line
                ld      hl,(SymHandle)
                inc     hl
                ld      a,h
                or      l
                jr      z,.no_label

                ; Process the rest of the line to find the value
                dec     hl
                ex      de,hl                   ; HL = input
                call    symGet                  ; DE = symbol info
                ex      de,hl                   ; DE = input, HL = symbol address

                ld      bc,hl
                call    exprGet                 ; HL = 16-bit expression value
                ret     c

                ld      a,SYM_VALUE
                add     bc,a
                ld      a,l
                ldi     (bc),a
                ld      a,h
                ld      (bc),a
                jp      asmTestEnd

.no_label:
                call    errorPrint
                dz      "LABEL NOT SUPPLIED"
                scf


;;----------------------------------------------------------------------------------------------------------------------
;; asmTestEnd
;;      Test that we parsed the whole line other produce a spurious error.
;; asmSpuriousError
;;      Called whenever a line is not terminated correctly.
;;

asmTestEnd:
                ld      a,(de)
                and     a
                ret     z

asmSpuriousError:
                ; DE = tokens that are spurious
                call    pageVideo
                ld      a,kInkError
                call    setColour
                call    print
                dz      "SPURIOUS TEXT AT END OF LINE: "
                ld      a,kInkNormal
                call    setColour

                ex      de,hl
                ld      de,AsmBuffer
                ld      a,(ReadOpcode)
                ld      c,a
                call    detokenise.entry
                ld      a,kInkError
                call    setColour
                ld      hl,AsmBuffer
                ld      a,kInkResponse
                call    setColour
                call    printHL
                ld      a,kInkNormal
                call    setColour
                ld      a,C_ENTER
                call    printChar
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmSkipLine
;;      Parse the whole line
;;
;; Input:
;;      DE = current lexed line (on 256-byte aligned buffer)
;;
;; Output:
;;      DE = address of null terminator
;;

asmSkipLine0:
                inc     e
asmSkipLine:
                ld      a,(de)
                and     a
                ret     z
                cp      T_STRING
                call    z,asmSkipLine0  ; We assume that no T_STRING chars are in the string
                cp      T_SYMBOL
                call    z,asmSkipLine0
                inc     e
                jp      asmSkipLine

;;----------------------------------------------------------------------------------------------------------------------
;; asmEmitOp_P
;; Emit (C << 4) | B as an opcode.  This is a common pattern in the assembler.  C is a 2 bit value that is shifted into
;; the P field with B filling the X, Q and Z fields of the opcode.
;;

asmEmitOp_P:
                ld      a,c
                swap
                or      b
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; asmEmitOp_Y
;; Emit (C << 3) | B as an opcode.  This is a common pattern in the assembler.  C is a 3 bit value that is shifted into
;; the Y field with B filling the X and Z fields of the opcode.
;;

asmEmitOp_Y:
                ld      a,c
                add     a,a
                add     a,a
                add     a,a
                or      b
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; asmINC
;; Handle all INC and DEC opcodes.  INC has base opcode of $00, DEC has $09
;;
;; INC rr               00 RR0 011
;; DEC rr               00 RR1 011
;; INC r                00 RRR 100
;; DEC r                00 RRR 101

asmINC:
                ld      h,OP_SP
                call    asm16bitReg_0   ; Test for BC, DE, HL, IX, IY or SP
                jr      nz,.not_rr

                ; INC/DEC RR
                ld      a,b
                or      $03             ; INC: 00000011, DEC: 00001011
                ld      b,a
                jp      asmEmitOp_P

.not_rr:
                call    asm8Bit
                jr      nz,.error
                jr      c,.indexed

                ; INC/DEC R
.write_opcode:
                ld      a,$04           ; 00000100
                or      b               ; 0000x10x
                and     7               ; 0000010x
                ld      b,a
                jp      asmEmitOp_Y

.indexed:
                call    .write_opcode
                ld      a,c
                cp      6               ; (IX/IY)?
                jr      nz,.done
                ld      b,l
                jp      emitB
.done:
                and     a
                ret

.error:
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmEX
;; Handle all the EX opcodes
;;
;; EX AF,AF'            00001000
;; EX (SP),HL/IX/IY     11100011
;; EX DE,HL             11101011
;;

asmEX:
                inc     e
                cp      OP_AF
                jr      nz,.not_af

                ; EX AF,AF'
                call    asmCheckSeq
                dz      ',',OP_AFA
                ret     c
                ld      a,$08
                jp      emitByte

.not_af:
                cp      '('
                jr      nz,.not_sp

                ; EX (SP),HL
                call    asmCheckSeq
                dz      OP_SP,')',','
                ret     c
                ld      a,(de)
                call    asmHL_IX_IY
                scf
                ret     nz              ; Error if not HL,IX or IY
                inc     e
                ld      a,$e3
                jp      emitByte

.not_sp:
                cp      OP_DE
                scf
                ret     nz

                ; EX DE,HL
                call    asmCheckSeq
                dz      ',',OP_HL
                ret     c
                ld      a,$eb
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; asmADD
;; Handle all the ADD RR,.. opcodes, otherwise hand off to asmALU
;;
;; ADD [A,]nn           Handled by asmALU
;; ADD [A,]r            Handled by asmALU
;; ADD HL/IX/IY,RR      00RR1001
;; ADD HL,A             ED 31
;; ADD DE,A             ED 32
;; ADD BC,A             ED 33
;; ADD HL,nnnn          ED 34 nn nn
;; ADD DE,nnnn          ED 35 nn nn
;; ADD BC,nnnn          ED 36 nn nn
;;

asmADD:
                ld      h,a                     ; Save the first token
                call    asmHL_IX_IY
                jp      nz,.not_hl
                jr      nc,.not_indexed

                ; ADD IX/IY,RR
                inc     e
                call    asmAcceptComma
                ret     c
.add_hl_rr
                call    asm16bitReg_1
                scf
                ret     nz
                ld      b,%00001001
                jp      asmEmitOp_P

                ; ADD HL,RR, ADD HL,A or ADD HL,nnnn
.not_indexed:
                inc     e
                call    asmAcceptComma
                ret     c
                ld      b,$34
                call    exprIsNext
                jr      nc,.add_rr_nnnn

                ; ADD HL,A
                ld      b,$31
                cp      OP_A
                jr      nz,.add_hl_rr

.add_rr_a:
                inc     e
                jp      asmEDOpcode

.add_rr_nnnn:
                ; ADD rr,nnnn
                call    exprGet
                ret     c
                call    asmEDOpcode
                jp      emitWord

.not_hl:
                cp      OP_DE
                jr      nz,.not_de

                inc     e
                call    asmAcceptComma
                ret     c
                ld      b,$35
                call    exprIsNext
                jr      nc,.add_rr_nnnn
                ld      b,$32
                cp      OP_A
                jp      z,.add_rr_a
                scf
                ret

.not_de:
                cp      OP_BC
                jp      nz,asmALU

                inc     e
                call    asmAcceptComma
                ret     c
                ld      b,$36
                call    exprIsNext
                jr      nc,.add_rr_nnnn
                ld      b,$33
                cp      OP_A
                jp      z,.add_rr_a
                scf
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; asmADC
;; Handle al the ADC RR,... opcodes, otherwise hand of to asmALU
;;
;; ADC [A,]nn           Handled by asmALU
;; ADC [A,]r            Handled by asmALU
;; ADC HL/IX/IY,RR      ED 01RR1010

asmADC:
                cp      OP_HL
                jp      nz,asmALU

                ; ADC HL,RR
                inc     e
                call    asmAcceptComma
                ret     c
                call    asm16bitReg_1_NoIndex
                scf
                ret     nz
                ld      a,$ED
                call    emitByte
                ld      b,%01001010
                jp      asmEmitOp_P

;;----------------------------------------------------------------------------------------------------------------------
;; asmSBC
;; Handle al the SBC RR,... opcodes, otherwise hand of to asmALU
;;
;; SBC [A,]nn           Handled by asmALU
;; SBC [A,]r            Handled by asmALU
;; SBC HL,RR            ED 01RR0010

asmSBC:
                cp      OP_HL
                jp      nz,asmALU

                ; ADC HL,RR
                inc     e
                call    asmAcceptComma
                ret     c
                call    asm16bitReg_1_NoIndex
                scf
                ret     nz
                ld      a,$ED
                call    emitByte
                ld      b,%01000010
                jp      asmEmitOp_P

;;----------------------------------------------------------------------------------------------------------------------
;; asmDJNZ
;; Handle DJNZ opcode.
;;

asmDJNZ:
                call    exprIsNext
                jr      nc,.expr_found
.no_disp:
                call    errorPrint
                dz      "DISPLACEMENT VALUE EXPECTED"
                ret
.expr_found:
                call    exprGet                 ; HL = address to jump to
                ld      a,(Pass)
                and     a
                jr      z,.write_disp           ; Only do calculation on pass 1
                push    de
                ld      de,(PC)
                inc     de
                inc     de
                call    asmCalcDisp
                ld      l,a
                pop     de
                ret     c

.write_disp:
                ld      a,b
                jp      emitOpByte

;;----------------------------------------------------------------------------------------------------------------------
;; asmCalcDisp
;; Calculate a displacement value or detect out of range
;;
;; Input:
;;      HL = address
;;      DE = from
;;
;; Output:
;;      Success:
;;              CF = 0
;;              A = displacement byte
;;      Fail:
;;              CF = 1
;;

asmCalcDisp:
                and     a
                sbc     hl,de           ; HL = displacement
                and     a               ; Clear CF

                ; Continue into next function

;;----------------------------------------------------------------------------------------------------------------------
;; asmValidDisp
;; Checks to see if a displacement in HL is valid (i.e. between -128 and +127).
;;
;; Input:
;;      HL = displacement
;;
;; Output:
;;      CF = 1 if error
;;      If CF = 0, L is valid displacement byte
;;

asmValidDisp:
                ld      a,l
                inc     h               ; Valid negative displacement has H == $ff
                jr      z,.neg
                dec     h               ; Valid positive displacement has H == $00
                jr      nz,.oor         ; Otherwise out of range!

                ; Handle positive values
                bit     7,a             ; Bit 7 must be 0 otherwise out of range
                ret     z               ; Everything's OK!
.oor:
                call    errorPrint
                dz      "RELATIVE JUMP OUT OF RANGE"
                ret
.neg:
                bit     7,a             ; Bit 7 must be 1 otherwise out of range
                ret     nz
                jr      .oor

;;----------------------------------------------------------------------------------------------------------------------
;; asmJR
;; Handles all JR opcodes
;;

asmJR:
                call    exprIsNext
                jr      c,.no_disp
                jp      asmDJNZ.expr_found
.no_disp:
                call    asmCC
                ret     c
                bit     5,a
                scf
                ret     nz
                or      $20             ; A = opcode
                ld      b,a
                call    asmAcceptComma
                jp      asmDJNZ.expr_found

;;----------------------------------------------------------------------------------------------------------------------
;; asmALU
;; Handles all ALU based opcodes
;;
;; <ALU-op> [A,]nn      11AAA110 nnnnnnnn
;; <ALU-op> [A,]r       10AAARRR

asmALU:
                ; DE -> first token
                cp      OP_A
                jr      z,.remove_a

.do_alu:
                ; DE -> position of first token
                ; A -> token of 2nd parameter
                call    exprIsNext
                jr      c,.simple

                ; ALU n
                call    exprGet
                ret     c
                ld      a,$c6
                or      b
                jp      emitOpByte

                ; ALU A,r
.simple:
                call    asm8Bit                 ; C = register index
                jr      nz,.error

                ; ZF = 1, CF = 0 if normal register, 1 if indexed.
                jr      nc,.write_opcode

                call    .write_opcode
                ld      a,c
                cp      6
                jr      nz,.ok                  ; Not (IX/IY+n)

                ld      b,l
                jp      emitB

.write_opcode:
                ld      a,b
                or      c
                jp      emitByte

.ok:
                and     a
                ret

.error:
                scf
                ret

.remove_a:
                ; Check to see we have a "A," prefix
                inc     e               ; DE points to: , or 0
                ; DE -> possible ',' or not
                ld      a,(de)
                cp      ','
                jr      nz,.one_param
                inc     e               ; DE -> second parameter
                ld      a,(de)
                jr      .do_alu
.one_param:
                dec     e
                ld      a,(de)
                jr      .do_alu

;;----------------------------------------------------------------------------------------------------------------------
;; asmRET
;; Handles all RET opcodes

asmRET:
                and     a
                jr      nz,.cond
                ld      a,$c9
                jp      emitByte
.cond:
                call    asmCC
                or      b
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; asmPUSH
;; Handles PUSH and POP opcodes
;;

asmPUSH:
                call    exprIsNext
                jr      nc,asmPOP.not_reg

asmPOP:
                ld      h,OP_AF
                call    asm16bitReg_0
                scf
                ret     nz
                jp      asmEmitOp_P
.not_reg:
                ; PUSH nnnn
                call    exprGet
                ret     c
                ld      b,$8a
                call    asmEDOpcode
                ld      b,h
                call    emitB
                ld      b,l
                jp      emitB
                

;;----------------------------------------------------------------------------------------------------------------------
;; asmBranch
;; Handles CALL and JP opcodes
;;

asmCALL:
                call    exprIsNext
                jp      c,asmBranch
                ld      b,$cd
                jr      asmBranch.write
asmJP:
                cp      '('                     ; Test for JP (HL/IX/IY) or JP (C)
                jr      z,.maybe_hl
                call    asmHL_IX_IY             ; Test for JP HL
                jr      z,.is_hl

.try_expr:
                call    exprIsNext              ; Test for JP nnnn
                jp      c,asmBranch             ; Otherwise JP cc,nnnn
                ld      b,$c3
                jr      asmBranch.write
.maybe_hl:
                inc     e
                ld      a,(de)
                cp      OP_C
                jr      z,.jp_c
                call    asmHL_IX_IY
                jr      nz,.try_expr
                inc     e
                ld      c,')'
                call    asmCheck
                ret     c
.is_hl:
                ld      a,$e9
                jp      emitByte

.jp_c:
                ld      b,$98
                call    asmEDOpcode
                call    asmCheckSeq
                dz      OP_C,')'
                ret

asmBranch:
                call    asmCC
                ret     c
                or      b
                ld      b,a
                call    asmAcceptComma
                ret     c
.write:
                call    exprGet
                ret     c
                ld      a,b
                jp      emitOpAddr

;;----------------------------------------------------------------------------------------------------------------------
;; asmRST
;; Handles the 8 RST opcodes
;;

asmRST:
                call    exprGet
                ret     c
                ld      a,h
                and     a
                scf
                ret     nz
                ld      a,l
                and     %11000111
                scf
                ret     nz
                ld      a,l
                or      b
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; asmROT
;; Handle all the shift and rotate opcodes
;;
;; <ROT-op>     CB 00IIIRRR     I = rotate/shift instruction, R = register

asmROT:
                call    asm8Bit
                jr      nz,.error
                jr      c,.indexed

                push    bc
                ld      b,$cb
                call    emitB
                pop     bc
                
.write_op:
                ld      a,b
                or      c               ; B = opcode
                jp      emitByte

.indexed:
                push    bc
                ld      b,$cb
                call    emitB
                ld      b,l
                call    emitB
                pop     bc
                jp      .write_op
.error:
                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmBIT
;; Handle all the BIT, RES and SET opcodes
;;
;; BIT n,r      CB 01nnnrrr
;; RES n,r      CB 10nnnrrr
;; SET n,r      CB 11nnnrrr

asmBIT:
                ; Get bit value
                call    exprGet
                ret     c
                ld      a,l
                cp      8
                ret     nc
                add     a,a
                add     a,a
                add     a,a
                or      b
                ld      b,a             ; B = ??NNN000

                ; Get the register
                call    asmAcceptComma
                ret     c

                ; B = opcode base
                jp      asmROT

;;----------------------------------------------------------------------------------------------------------------------
;; asmOUT
;; Handle the OUT opcodes: 
;;      OUT (n),A       D3 NN
;;      OUT (C),r       ED 01RRR001
;;      OUT (C),0       ED 01110001 (RRR == 110)
;;

asmOUT:
                ld      c,'('
                call    asmCheck
                ret     c
                cp      OP_C
                jr      z,.outc

                ; OUT (n),A

                call    exprGet
                ret     c
                ld      a,(Pass)
                and     a
                jr      z,.no_check_port
                ld      a,h
                and     a
                jr      nz,asmBadPort

.no_check_port:
                call    asmCheckSeq
                dz      ')', ',', OP_A
                ret     c

                ld      a,$d3
                jp      emitOpByte

.outc:
                inc     e
                ld      c,')'
                call    asmCheck
                ret     c
                call    asmAcceptComma
                ret     c
                
                call    exprIsNext
                jr      nc,.out0

                ; OUT (C),r
                call    asm8Bit
                ret     c               ; Return on error or if indexed
                cp      6
                scf
                ret     z
                ld      a,c
                add     a,a
                add     a,a
                add     a,a
                or      b
                jp      emitEDOp

                ; OUT (C),0
.out0:
                call    exprGet
                ld      a,h
                or      l
                scf
                ret     nz              ; Must be 0!
                ld      a,$71
                jp      emitEDOp
asmBadPort:
                call    errorPrint
                dz      "PORT NUMBER OUT OF RANGE"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; asmIN
;; Handle the IN opcodes:
;;      IN A,(n)        DB NN
;;      IN r,(C)        ED 01RRR000
;;      IN (C)          ED 01110000
;;

asmIN:
                cp      OP_A
                jr      z,.ina

                call    asm8Bit
                jr      nz,.inc
                ret     c               ; Error if indexed
                cp      6
                scf
                ret     z               ; IN (HL),(C) is invalid

                ; IN R,(C)
                ld      a,c
                add     a,a
                add     a,a
                add     a,a             ; A = 00RRR000
                or      b               ; A = 01RRR000
                ld      b,a
                call    asmAcceptComma
                ret     c
.inc_write:
                call    asmCheckSeq
                dz      '(',OP_C,')'
                ret     c
                ld      a,b
                jp      emitEDOp

.inc:
                ; IN (C)
                ld      b,$70
                jr      .inc_write

.ina:
                ; IN A,(n)
                inc     e
                call    asmAcceptComma
                ret     c
                ld      c,'('
                call    asmCheck
                ret     c
                call    exprIsNext
                jr      c,.in_a_c
                call    exprGet
                ret     c
                ld      a,(Pass)
                and     a
                jr      z,.no_check
                ld      a,h
                and     a
                jr      nz,asmBadPort
.no_check:
                ld      c,')'
                call    asmCheck
                ret     c

                ld      a,$db
                jp      emitOpByte

.in_a_c:
                call    asmCheckSeq
                dz      OP_C,')'
                ret     c
                ld      c,$78
                jp      emitEDOpC

;;----------------------------------------------------------------------------------------------------------------------
;; Handle the IM opcode
;;
;;      IM 0    ED 01000110
;;      IM 1    ED 01010110
;;      IM 2    ED 01011110
;;

asmIM:
                call    exprIsNext
                ret     c
                call    exprGet
                ret     c

                ld      a,h
                and     a
                scf
                ret     nz

                ; IM 0
                ld      a,l
                ld      c,$46
                and     a
                jp      z,emitEDOpC

                ; IM 1
                dec     a
                ld      c,$56
                jp      z,emitEDOpC

                ; IM 2
                dec     a
                ld      c,$5e
                jp      z,emitEDOpC

                scf
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Handle the MIRROR opcode
;;
;;      mirr    [a]     ED 24
;;      mirror  [a]     ED 24
;;

asmMirror:
                cp      OP_A
                jr      nz,.done
                inc     e               ; Consume the A operand
.done:
                jp      asmEDOpcode

;;----------------------------------------------------------------------------------------------------------------------
;; Handle the TEST opcode
;;
;;      test nn         ED 27 nn
;;

asmTest:
                call    exprIsNext
                ret     c
                call    exprGet
                ret     c

                ; Ensure 8-bit
                ld      a,h
                and     a
                scf
                ret     nz

                call    asmEDOpcode
                ld      b,l
                jp      emitB

;;----------------------------------------------------------------------------------------------------------------------
;; Handle the Bit shift opcodes
;;
;;      bsla de,b       ED 28
;;      bsra de,b       ED 29
;;      bsrl de,b       ED 2A
;;      bsrf de,b       ED 2B
;;      brlc de,b       ED 2C
;;

asmBitShift:
                call    asmCheckSeq
                dz      OP_DE,',',OP_B
                ret     c
                jp      asmEDOpcode

;;----------------------------------------------------------------------------------------------------------------------
;; Handle the nextreg opcode
;;
;;      nreg rr,nn      ED 91 rr nn
;;      nreg rr,a       ED 92 rr
;;

asmNextreg:
                call    exprIsNext
                ret     c
                call    exprGet
                ret     c
                ld      a,h
                and     a
                scf
                ret     nz

                ; L = register
                call    asmAcceptComma
                ret     c

                cp      OP_A
                jr      nz,.not_a

                ; nreg rr,a
                inc     e               ; Consume A
                inc     b
                call    asmEDOpcode
                ld      b,l
                jp      emitB
.not_a:
                ; nreg rr,nn
                call    asmEDOpcode
                ld      b,l
                call    emitB

                ld      a,(de)
                call    exprIsNext
                ret     c
                call    exprGet
                ret     c
                ld      a,h
                and     a
                scf
                ret     nz
                ld      b,l
                jp      emitB

;;----------------------------------------------------------------------------------------------------------------------
;; Handle all single opcode ED instructions
;;
;; Input:
;;      B = opcode
;;

asmEDOpcode:
                ld      c,b
                jp      emitEDOpC

;;----------------------------------------------------------------------------------------------------------------------
;; Handle DEFB and DEFZ
;;
;; Input:
;;      B will be $01 for DEFZ and $02 for DEFC.
;;

asmDEFB:
                push    bc                      ; Store B
                xor     a
                ld      (emitDelay.filled),a
.next_byte:
                ld      a,(de)
                cp      T_STRING
                jr      z,.string
                
                call    exprIsNext
                jr      c,.not_expr

                ; Handle expression
                call    exprGet                 ; HL = expression
                jr      c,.error
                ld      a,h
                and     a
                jr      z,.is_8bit

                ; Expression is not 8-bit!
                call    errorPrint
                dz      "EXPRESSION IS NOT 8-BIT"
.error:
                pop     bc
                ret

.is_8bit:
                ld      b,l
                call    emitDelay

.continue:
                call    asmAcceptComma
                jr      nc,.next_byte           ; Next byte!

                ; Check to see if DEFZ or DEFB was used
                pop     bc
                ld      a,b
                and     a
                jr      z,.emit_last            ; DEFB was used, just exit

                cp      1                       ; DEFZ was used?
                jr      nz,.defc
                ld      b,0
                call    emitDelay               ; DEFZ was used, add a null byte
.emit_last:
                jp      emitDelay               ; Emit one more byte
.defc:
                ld      a,(emitDelay.byte)
                or      $80
                ld      (emitDelay.byte),a
                jp      emitDelay

.string:
                ; Handle strings
                inc     de
.l0:
                ldi     a,(de)                  ; Get next character in string
                and     a
                jr      z,.continue             ; End of string?  Leave loop if so.

                ld      b,a
                call    emitDelay                ; Write character
                jr      .l0

.not_expr:
                pop     bc
                call    errorPrint
                dz      "INVALID PARAMETER"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Emit byte in B but on next call for this.  This allows the B to be processed when certain conditions
;; arise.
;;
;; Input:
;;      B = byte to emit
;;

emitDelay:
                ld      a,(.byte)               ; A = last byte
                ld      c,a                     ; C = last byte
                ld      a,b
                ld      (.byte),a               ; Store current byte

                ld      a,(.filled)
                and     a                       ; Have we recevied a byte yet
                jr      z,.no_byte

                ; OK C = byte to emit
                ld      b,c
                jp      emitB                   ; Emit it
.no_byte:
                ld      a,1
                ld      (.filled),a
                ret

.byte           db      0
.filled         db      0

;;----------------------------------------------------------------------------------------------------------------------
;; Handle DEFW
;;

asmDEFW:
                call    exprIsNext
                jr      c,.not_expr

                ; Handle expression
                call    exprGet                 ; HL = expression
                ret     c
                call    emitWord
                ret     c

                call    asmAcceptComma
                jr      nc,asmDEFW              ; Next word!
                and     a                       ; Clear CF since no comma is valid.

                ret

.not_expr:
                call    errorPrint
                dz      "INVALID PARAMETER"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Handle DEFS

asmDEFS:
                call    exprIsNext
                jr      c,.not_expr

                ; Get size
                call    exprGet                 ; HL = count
                ret     c
                xor     a
                ld      b,a                     ; B = character to write into buffer
                ld      a,(de)
                cp      ','
                jr      nz,.no_comma

                ; Check for byte value
                push    hl
                call    asmAcceptComma
                call    exprIsNext
                pop     hl
                jr      c,.not_expr
                push    hl
                call    exprGet
                jr      c,.error_pophl
                ld      a,h
                and     a
                jr      nz,.not8bit
                ld      b,l
                pop     hl
.no_comma:
                ld      a,h
                or      l
                ret     z

                ; Create the buffer
                call    emitB
                dec     hl
                jp      .no_comma

.not_expr:
                call    errorPrint
                dz      "EXPRESSION EXPECTED"
                ret

.not8bit:
                call    errorPrint
                dz      "EXPECTED 8-BIT VALUE"
.error_pophl:
                pop     hl
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Handle INCBIN
;;

asmINCBIN:
                cp      T_STRING
                jr      z,.is_string
.not_filename:
                call    errorPrint
                dz      "FILENAME EXPECTED"
                ret

.is_string:
                inc     de

                ; Read in the file and emit the bytes
                ex      de,hl

                ;;
                ;; Try to open the file
                ;;

                ld      de,FileName
                call    strcpy
                ex      de,hl                   ; DE = after filename

                call    fOpenLoad               ; HL = file buffer
                jr      c,.error_open

                push    de

                ;;
                ;; Try to load the data
                ;;

.next_chunk:
                ; Attempt to read in 256 bytes into the file buffer
.next_byte:
                call    fReadByte
                jr      c,.error_read
                jr      z,.done
                call    emitByte
                jr      nc,.next_byte

.done:
                ;;
                ;; Close the file
                ;;

                push    af
                call    fClose
                pop     af,de
                ret

.error_open:
                call    errorPrint
                dz      "UNABLE TO OPEN FILE"
                ret
.error_read:
                call    errorPrint
                dz      "UNABLE TO READ FILE"
                jp      .done

.handle         db      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmINCLUDE
;; Handle the INCLUDE directive
;;

asmINCLUDE:
                push    de
                ld      de,(iy+AsmContext.LineNum)
                inc     de
                ld      (iy+AsmContext.LineNum),de
                pop     de
                
                ;; FORMAT:
                ;;      INCLUDE "filename"
                cp      T_STRING
                jp      nz,asmINCBIN.not_filename

                inc     de
                ex      de,hl
                ld      de,FileName
                call    strcpy
                ex      de,hl

                push    de
                call    asmPushDoc
                pop     de
                ret     c

                call    asmPageDoc

                ; Decrease the line number because (set it to $ffff)
                dec     (iy+AsmContext.LineNum+0)
                dec     (iy+AsmContext.LineNum+1)
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Handle ORG

asmORG:
                ;; FORMAT:
                ;;      ORG <physical address> [, <virtual address>]

                call    exprIsNext
                jr      c,.err_1
                call    exprGet         ; HL = physical address
                jr      c,.err_1
                call    checkAddress
                jr      c,.err_2

                ld      (WritePC),hl
                call    asmAcceptComma
                jr      nc,.virt
                ld      (PC),hl
                ret
.virt
                call    exprIsNext
                jr      c,.err_1
                call    exprGet         ; HL = virtual address
                jr      c,.err_1
                ld      (PC),hl
                ret
.err_1:
                call    errorPrint
                dz      "EXPECTED 16-BIT ADDRESS"
                ret
.err_2:
                call    errorPrint
                dz      "INVALID ADDRESS"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; checkAddress
;; Based on current mode, ensure the address is a good range
;;
;; Input:
;;      HL = address
;;
;; Output:
;;      CF = 1 if invalid address

checkAddress:
                push    de
                ld      de,$8000
                call    compare16               ; HL < $8000?
                pop     de
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; Handle SAVE
;;
;; Format:
;;      SAVE [+]"filename",[<address>,<size>]
;;

asmSAVE:
                ; Only works on Pass 1
                ld      a,(Pass)
                dec     a
                jp      nz,asmSkipLine

                ; Initialise the default arguments
                push    de
                ld      hl,$8000
                ld      (.start),hl
                ex      de,hl
                ld      hl,(WritePC)
                and     a
                sbc     hl,de
                pop     de
                ld      (.length),hl

                ; Check for append
                ld      a,(de)
                cp      '+'
                ld      a,0
                jr      nz,.not_append
                inc     a
                inc     de
.not_append:    ld      (.append),a

                ; Check for filename and copy it into the FileName buffer
                ld      a,(de)
                cp      T_STRING
                jr      nz,.err_no_fname
                inc     e
                ex      de,hl
                ld      de,FileName
                call    strcpy
                ex      de,hl

                ; Do we have a comma (and hence and address and size following)
                call    asmAcceptComma
                jr      c,.save

                ; Process address
                call    exprIsNext
                jr      c,.err_expect_addr
                call    exprGet
                jr      c,.err_expect_addr
                ld      (.start),hl

                ; Process length
                call    asmAcceptComma
                jr      c,.err_expect_len
                call    exprIsNext
                jr      c,.err_expect_len
                call    exprGet
                jr      c,.err_expect_len
                ld      (.length),hl

.save:
                ld      hl,(.start)
                ld      bc,(.length)
                ld      a,(.append)
                exa
                jp      asmSaveCode

.err_no_fname:
                call    errorPrint
                dz      "EXPECTED FILENAME"
                ret
.err_expect_addr:
                call    errorPrint
                dz      "EXPECTED START ADDRESS"
                ret
.err_expect_len:
                call    errorPrint
                dz      "EXPECTED END ADDRESS"
                ret

.start          dw      0
.length         dw      0
.append         db      0

;;----------------------------------------------------------------------------------------------------------------------
;; asmSaveCode
;; Save data in the virtual window
;;
;; Input:
;;      HL = start address (in 64K window)
;;      BC = length
;;      FileName buffer = file name to create (or append to)
;;      A' = 1 if append
;;      

asmSaveCode:
                push    de

                ; Check to see if parameters are good
                ld      de,hl
                add     hl,bc
                call    compare16
                jp      c,.err_too_big          ; Error if end address is before start address
                call    checkAddress
                jp      c,.err_invalid_addr     ; Error if end address is invalid
                ex      de,hl
                call    checkAddress            ; Error if start address is invalid
                jr      c,.err_invalid_addr

                ; Open the file
                push    hl
                exa
                and     a
                jr      nz,.append
                call    fOpenSave
                jr      .cont
.append:
                call    fOpenAppend
.cont:
                pop     de
                jp      c,.err_open

                ; DE = start address
                ; BC = length

                call    readPage4
                ld      (.old_page),a

.l0:
                ; Check to see if all work has been done
                ld      a,b
                or      c
                jr      z,.done

                ; Page in the current slot
                ld      a,d
                and     $e0
                swap
                rrca                    ; A = slot number
                ld      hl,MMUState
                add     hl,a
                ld      a,(hl)          ; A = page to save
                page    4,a

                push    bc,de           ; Save current parameters

                ; Calculate how many bytes from current address to write from page
                ld      hl,de
                ld      a,h
                and     $1f
                ld      h,a             ; HL = offset into page 4
                push    hl              ; Store this address
                ex      de,hl
                ld      hl,$2000
                sbc     hl,de           ; HL = $2000-(addr & $1fff)
                ld      de,bc           ; DE = work left
                call    max             ; DE = actual work we should do (min of bytes left and bytes left in page)

                pop     hl              ; HL = current address (in page 4)
                ld      a,h
                add     $80
                ld      h,a             ; HL = real address in page 4
                ld      bc,de           ; BC = actual work to do
                call    fWrite
                jr      c,.err_write
                pop     hl              ; HL = start address
                add     hl,bc           ; HL = new start address
                ex      de,hl           ; DE = new address
                and     a
                pop     hl              ; Get old number of bytes left to do
                sbc     hl,bc           ; HL = number of bytes left to do
                ld      bc,hl
                jr      .l0

.done:
                call    fClose
.done_no_close:
                ld      a,(.old_page)
                page    4,a
                pop     de
                ret

.err_invalid_addr:
                call    errorPrint
                dz      "INVALID ADDRESS"
                jr      .err_end

.err_too_big:
                call    errorPrint
                dz      "NUMBER OF BYTES TO SAVE TOO LARGE"
.err_end:
                pop     bc
                jr      .done

.err_open:
                call    errorPrint
                dz      "UNABLE TO OPEN FILE TO SAVE"
                jr      .done_no_close
.err_write:
                call    errorPrint
                dz      "UNABLE TO WRITE TO FILE"
                jr      .err_end


.old_page:      db      0


;;----------------------------------------------------------------------------------------------------------------------
;; INFORMATION:
;;
;; Parameters for Handlers:
;;      A = first parameter token
;;      DE = address of first parameter
;;      B = opcode base (used sometimes to generate bit patterns for opcodes)
;;
;;      HL & C are unused.
;;
;;      DE must finish on the terminating NULL at end
;;----------------------------------------------------------------------------------------------------------------------

;;----------------------------------------------------------------------------------------------------------------------
;; Assembler token tables
;;

; Provides an opcode base byte based on the token to help the handlers generate assembler code
OpcodeBaseTable:
                db      $88,$80,$a0,$40,$c4,$3f,$b8,$a9         ; $80
                db      $b9,$a1,$b1,$2f,$27,$09,$00,$01         ; $88
                db      $00,$00,$f3,$00,$10,$fb,$00,$00         ; $90
                db      $00,$d9,$76,$00,$40,$00,$aa,$ba         ; $98
                db      $a2,$b2,$c2,$18,$00,$a8,$b8,$a0         ; $a0
                db      $b0,$44,$00,$b0,$00,$bb,$b3,$41         ; $a8
                db      $ab,$a3,$c1,$c5,$80,$c0,$4d,$45         ; $b0
                db      $10,$17,$00,$07,$6f,$18,$1f,$08         ; $b8
                db      $0f,$67,$c7,$98,$37,$c0,$20,$28         ; $c0
                db      $38,$90,$a8,$30,$23,$24,$27,$28         ; $c8
                db      $29,$2a,$2b,$2c,$30,$90,$91,$93         ; $d0
                db      $94,$95,$a4,$a5,$ac,$b4,$b7,$bc         ; $d8
                db      $00,$00,$00,$02,$00,$00,$00,$00         ; $e0

AsmTokenTable:
                ; $80
                dw      asmADC
                dw      asmADD
                dw      asmALU
                dw      asmBIT
                dw      asmCALL
                dw      emitB
                dw      asmALU
                dw      asmEDOpcode
                ; $88
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      emitB
                dw      emitB
                dw      asmINC
                dw      asmDEFB
                dw      asmDEFB
                ; $90
                dw      asmDEFS
                dw      asmDEFW
                dw      emitB
                dw      asmOPT
                dw      asmDJNZ
                dw      emitB
                dw      asmUnknownToken
                dw      asmEQU
                ; $98
                dw      asmEX
                dw      emitB
                dw      emitB
                dw      asmIM
                dw      asmIN
                dw      asmINC
                dw      asmEDOpcode
                dw      asmEDOpcode
                ; $A0
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmJP
                dw      asmJR
                dw      asmLD
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                ; $A8
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      emitB
                dw      asmALU
                dw      asmORG
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmOUT
                ; $B0
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmPOP
                dw      asmPUSH
                dw      asmBIT
                dw      asmRET
                dw      asmEDOpcode
                dw      asmEDOpcode
                ; $B8
                dw      asmROT
                dw      emitB
                dw      asmROT
                dw      emitB
                dw      asmEDOpcode
                dw      asmROT
                dw      emitB
                dw      asmROT
                ; $C0
                dw      emitB
                dw      asmEDOpcode
                dw      asmRST
                dw      asmSBC
                dw      emitB
                dw      asmBIT
                dw      asmROT
                dw      asmROT
                ; $C8
                dw      asmROT
                dw      asmALU
                dw      asmALU
                dw      asmROT
                dw      asmEDOpcode
                dw      asmMirror
                dw      asmTest
                dw      asmBitShift
                ; $D0
                dw      asmBitShift
                dw      asmBitShift
                dw      asmBitShift
                dw      asmBitShift
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmNextreg
                dw      asmEDOpcode
                ; $D8
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                dw      asmEDOpcode
                ; $E0
                dw      asmINCBIN
                dw      asmINCLUDE
                dw      asmSAVE
                dw      asmDEFB
                dw      asmUnknownToken
                dw      asmUnknownToken
                dw      asmUnknownToken
                dw      asmUnknownToken


;;----------------------------------------------------------------------------------------------------------------------
;; Some errors

invalidInst:
                call    errorPrint
                dz      "INVALID INSTRUCTION"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
