;;----------------------------------------------------------------------------------------------------------------------
;; Lexical Analyser
;; Reads a line and converts it into tokens
;;

;; $80+ matches the same tokens from token.s

LineEnd         equ     $

T_SYMBOL        equ     1       ; Next bytes are characters followed by null terminator
T_VALUE         equ     2       ; Next word is the value
T_DOLLAR        equ     3       ; Token for '$'
T_STRING        equ     4       ; Used for strings

T_EXPR          equ     5       ; maximum special expression value + 1


;;----------------------------------------------------------------------------------------------------------------------
;; Lexical analyser
;;
;; Input:
;;      DE = input
;;
;; Output:
;;      AsmBuffer contains lexicons
;;      DE = points to start of next line in document
;;      CF = 1 if error occurs
;;      HL = points to line
;;

lex:
                ld      hl,AsmBuffer
.l1:
                call    lexSkipWS
                
                ;
                ; Check for EOL
                ;
                ld      a,(de)
                and     a
                jr      nz,.not_eol     ; Reached end of line?
.eol:
                xor     a
                call    writeLex
                ex      de,hl
                call    strEnd
                ex      de,hl
                inc     de
                ld      hl,AsmBuffer
                ret

.not_eol:
                ;
                ; Check for tokens
                ;
                cp      $80             ; Are we looking at a token?
                jp      c,.not_token
                ld      c,a             ; Save token
                inc     de
                ld      a,(de)
                cp      4
                jr      nc,.not_subtoken
                and     a
                ld      a,c
                jp      nz,.write       ; It's between 1 and 3 inclusive, skip byte and write
.not_subtoken:
                dec     de              ; Restore input position
                ld      a,c             ; Restore token (just in case it's not set)
                jp      .write

                ;
                ; Check for comments
                ;

.not_token
                cp      ';'
                jp      z,.eol

                ;
                ; Check for decimal digits
                ;
                call    isDigit
                jr      c,.not_digit
                sub     '0'
                ld      c,a
                ld      b,0             ; BC = current value
                inc     de
.digit_loop:
                ld      a,(de)
                call    isDigit
                jr      c,.end_value
                inc     de
                sub     '0'
                exa                     ; Store digit
                push    hl              ; Store current position in Lex output
                sla     c
                rl      b               ; BC*2
                ld      hl,bc
                sla     c
                rl      b               ; BC*4
                sla     c
                rl      b               ; BC*8
                add     hl,bc
                ld      bc,hl           ; BC*10
                pop     hl
                exa
                add     bc,a            ; BC = updated value
                jr      .digit_loop
.end_value:
                ; BC = value
                ld      a,T_VALUE
                call    writeLex
                ret     c
                call    writeLexWord
                ret     c
                jp      .l1

.not_digit:
                ;
                ; Check for labels
                ;
                call    isIdentChar     ; Label?
                jr      c,.not_id
                ld      a,T_SYMBOL
                call    writeLex
                ret     c

.sym_loop:
                ld      a,(de)
                call    isIdentChar
                jr      c,.endsym
                call    writeLex
                ret     c
                inc     de
                jr      .sym_loop
.endsym:
                xor     a
                call    writeLex
                ret     c
                call    lexSkipWS
                cp      ':'
                jp      nz,.l1          ; Compute next lexicon
                inc     de              ; Ignore colon
                jp      .l1

                ;
                ; Check for hexadecimal digits and current PC
                ;
.not_id:
                cp      '$'
                jr      nz,.not_hex
                inc     de
                ld      a,(de)
                call    isHexDigit
                jr      c,.dollar
                ld      bc,0
.hex_loop:
                call    hexChar
                sla     c
                rl      b
                sla     c
                rl      b
                sla     c
                rl      b
                sla     c
                rl      b
                add     bc,a            ; BC = new value

                inc     de
                ld      a,(de)
                call    isHexDigit
                jr      nc,.hex_loop
                jr      .end_value
.dollar:
                dec     de
                ld      a,T_DOLLAR
                jp      .write

                ;
                ; Check for binary digits
                ; 
.not_hex:
                cp      '%'
                jr      nz,.not_binary
                ld      bc,0
                inc     de
                ld      a,(de)
                cp      '0'
                jr      z,.is_bit
                cp      '1'
                jp      nz,.syntax_error
                jr      .is_bit

.loop_binary:
                inc     de
                ld      a,(de)
                cp      '_'
                jr      z,.loop_binary
                cp      '0'
                jr      z,.is_bit
                cp      '1'
                jp      nz,.end_value
.is_bit:
                sub     $30             ; A = 0 or 1
                rrca                    ; CF = A
                rl      c
                rl      b
                jr      .loop_binary

                ;
                ; Check for strings
                ;
.not_binary:
                cp      '"'
                jr      nz,.not_string

.is_string
                ld      a,T_STRING
                call    writeLex
                ret     c
                inc     de
.string_loop:
                ldi     a,(de)
                cp      '"'
                jr      z,.end_string
                and     a
                jr      z,.error_string
                cp      $21
                jr      nc,.not_spaces
                sub     $22
                cp      $0a-$22
                ret     c           ; $00..$09 input (invalid)
                cpl                 ; A = number of spaces to fill for $0b..$20
                jr      nz,.not_0a_space
                ; Decompress $0a nn sequence
                ldi     a,(de)      ; $0a has count in next byte
.not_0a_space:
                ld      b,a
                ld      a,' '
.l2:            call    writeLex
                ret     c
                djnz    .l2
                jr      .string_loop
.not_spaces:
                call    writeLex
                ret     c
                jr      .string_loop
.error_string
                call    errorPrint
                dz      "UNTERMINATED STRING"
                ret
.end_string:
                xor     a
                call    writeLex
                jp      .l1

                ;
                ; Check for all other characters
                ;
.not_string:
                cp      $80
                jr      c,.write
                cp      $20
                jr      nc,.write

.not_punct
                jr      .syntax_error


.write:
                call    writeLex
                ret     c
                inc     de
                jp      .l1

.syntax_error:
                call    errorPrint
                dz      'SYNTAX ERROR'
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; lexSkipWS
;; Skip WS in the current line
;;
;; Input:
;;      DE = document
;;
;; Output:
;;      A = first non-space document char
;;      DE = address of first non-space char
;;

lexSkipWS:
                ld      a,(de)
                cp      ' '+1
                ret     nc              ; $21+ value -> not WS
                cp      $0a
                ret     c               ; $00..$09 -> not WS
                inc     de
                jr      nz,lexSkipWS    ; $0b..$20 skipped
                inc     de
                jr      lexSkipWS       ; <$0a,count> skipped


;;----------------------------------------------------------------------------------------------------------------------
;; writeLex
;; Write a byte to the Line buffer
;;
;; Input:
;;      HL = 1 byte before write position in line buffer
;;      A = character to write
;;
;; Output:
;;      CF = 0 if successful, otherwise 1 means buffer overrun
;;

writeLex:
        IF DEBUG_LEX
                push    af
                call    pageVideo
                call    printHexByte
                call    printSpace
                call    asmPageDoc
                pop     af
        ENDIF
                ld      (hl),a
                inc     l
                jr      z,.oom
                and     a
                ret

.oom:
                call    errorPrint
                dz      "LINE TOO COMPLEX"
                ret

;;----------------------------------------------------------------------------------------------------------------------
;; writeLexWord
;; Write a word to the line byffer
;;
;; Input:
;;      HL = 1 byte before write position in the line buffer
;;      BC = word to write
;;
;; Output:
;;      CF = 0 if successful
;;

writeLexWord:
                ld      a,c
                call    writeLex
                ret     c
                ld      a,b
                call    writeLex
                ret

