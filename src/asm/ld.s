;;----------------------------------------------------------------------------------------------------------------------
;; Handle for the LD instruction.  It's huge!  So requires its own source file
;;----------------------------------------------------------------------------------------------------------------------

R8_B            equ     $00     ; B
R8_C            equ     $01     ; C
R8_D            equ     $02     ; D
R8_E            equ     $03     ; E
R8_H            equ     $04     ; H
R8_L            equ     $05     ; L
R8_HL           equ     $06     ; (HL)
R8_A            equ     $07     ; A
R8_BC           equ     $08     ; (BC)
R8_DE           equ     $09     ; (DE)
R8_IXH          equ     $0a     ; IXH
R8_IXL          equ     $0b     ; IXL
R8_IYH          equ     $0c     ; IYH
R8_IYL          equ     $0d     ; IYL
R8_I            equ     $0e     ; I
R8_R            equ     $0f     ; R
R8_IX           equ     $10     ; (IX)
R8_IY           equ     $11     ; (IY)
R8_P_NNNN       equ     $12     ; (nnnn)
R8_NN           equ     $13     ; nn

R16_BC          equ     $00     ; BC
R16_DE          equ     $01     ; DE
R16_HL          equ     $02     ; HL
R16_SP          equ     $03     ; SP
R16_IX          equ     $04     ; IX
R16_IY          equ     $05     ; IY
R16_P_NNNN      equ     $06     ; (nnnn)
R16_NNNN        equ     $07     ; nnnn


;;----------------------------------------------------------------------------------------------------------------------
;; Recap:
;;
;; For all handlers we have these registers:
;;
;;      B = base opcode (this is irrelevant for LD)
;;      DE = address of first token after LD opcode
;;

asmLD:
                ; A = first token
                ; DE = address of next token

;;----------------------------------------------------------------------------------------------------------------------
;; First possible tokens after LD:
;;
;;      rr = BC, DE, HL, SP
;;      r = B, C, D, E, H, L, (HL), A
;;      nn = 16-bit value
;;      n = 8-bit value
;;
;;      SP                      LD SP,HL/IX/IY          (DD/FD) F9
;;
;;      BC, DE, HL, SP          LD rr,nn                (DD/FD) 00RR0001 nn nn
;;                              LD rr,(nn)              ED 01RR1011 nn nn
;;                                                      (DD/FD) 2A nn nn (for HL)
;;
;;      (                       LD (BC),A               02
;;                              LD (DE),A               12
;;                              LD (nn),HL              (DD/FD) 22 nn nn
;;                              LD (nn),A               32
;;                              LD (nn),rr              ED 01RR0011 nn nn
;;
;;      A                       LD A,(BC)               0A
;;                              LD A,(DE)               1A
;;                              LD A,(nn)               3A
;;                              LD A,I                  ED 57
;;                              LD A,R                  ED 5F
;;
;;      B, C, D, E, H, L, A,    LD r,r                  (DD/FD) 010RRR0RRR (nn)
;;      (HL), (IX+n), (IY+n)    LD r,n                  (DD/FD) 000RRR0110 (nn)
;;
;;      I                       LD I,A                  ED 47
;;
;;      R                       LD R,A                  ED 4F
;;

                call    ldGet8
                jr      c,.ld16bit              ; We expect a 16-bit load

                ; We look at the special case of LD (nnnn),A.  If we match (nnnn) but
                ; don't get an ,A following, then (nnnn) is a 16-bit source.
                ld      a,c
                cp      R8_P_NNNN
                jr      nz,.ld8bit              ; No? Definitely an 8-bit load.

                push    de
                call    asmCheckSeq
                dz      ',',OP_A
                pop     de
                ld      a,(de)
                jr      nc,.ld8bit

                ; It is 16-bit version of LD (nnnn)
                ld      b,R16_P_NNNN
                jr      .ld16bit_cont

                ; Handle 16-bit LD opcodes
.ld16bit:
                call    ldGet16                 ; C = operand index
                jp      c,invalidInst           ; Not a 16-bit load token, error!
                ld      b,c

.ld16bit_cont:
                call    asmAcceptComma
                ret     c

                call    ldGet16_nn
                ret     c

                ld      a,2*8
                ld      (.smc_width),a
                ld      ix,LDTable16
                push    bc
                call    .write
                pop     bc
                ret     c

                ; Test to see if we write a 16-bit address/value
                ld      a,b
                cp      R16_P_NNNN
                jp      nc,emitWord
                ld      a,c
                cp      R16_P_NNNN
                jp      nc,emitWord
                and     a
                ret

.ld8bit:
                ; HL = address or displacement
                ; Handle 8-bit LD opcodes
                ld      a,l
                ld      (.poss_disp),a
                ld      b,c

                call    asmAcceptComma
                ret     c

                call    ldGet8_nn
                ret     c

                ld      a,2*20
                ld      (.smc_width),a
                ld      ix,LDTable8
                push    bc
                call    .write
                pop     bc
                ret     c

                ; Check to see if we need to write out an address
                ld      a,c
                cp      R8_P_NNNN
                jp      z,emitWord
                ld      a,b
                cp      R8_P_NNNN
                jp      z,emitWord
                
.no_addr:
                cp      R8_IX
                jr      z,.disp1
                cp      R8_IY
                jr      z,.disp1
                ld      a,c
                cp      R8_IX
                jr      z,.disp2
                cp      R8_IY
                jr      nz,.no_disp
.disp2:
                ld      b,l
                call    emitB
                jr      .no_disp
.disp1:
                ld      a,(.poss_disp)
                call    emitByte
                ld      a,c
.no_disp:
                cp      R8_NN
                jr      nz,.no_arg
                ld      b,l
                jp      emitB
.no_arg:
                and     a
                ret

.poss_disp      db      0

                ; Write 1 or 2 opcodes for the matching instruction
                ; B = first arg index
                ; C = second arg index
                ; IX = table
.write:
                ; Look up the table
                push    de
                ld      d,b
.smc_width      equ     $+1
                ld      e,8
                mul                             ; DE = row offset
                ld      a,c
                add     a,a
                add     de,a                    ; DE = location of 2-byte instruction bytes
                add     ix,de                   ; IX = opcode info
                pop     de

                ; Test first opcode (if 0 then instruction combination does not exist)
                ld      a,(ix+0)
                and     a
                scf
                ret     z                       ; If 0, invalid opcode
                call    emitByte

                ; Test second byte (if 0 then there is no 2nd byte)
                ld      a,(ix+1)
                and     a
                ret     z
                jp      emitByte

;;----------------------------------------------------------------------------------------------------------------------
;; ldGet8(_nn)
;; Convert the token to an index or set CF = 1 if error occurs
;;
;; Input:
;;      A = token
;;      DE = address of token
;;
;; Output:
;;      C = index of 8-bit reference
;;      DE = points after tokens if matched
;;      ZF = 1 if match occurs
;;      DE is unchanged if ZF = 0
;;      HL = address or displacement
;;

TEST_OP         macro   op
                inc     c
                cp      op
                ret     z
                endm

ldGet8_nn:
                call    exprIsNext
                jr      c,ldGet8

                ; Get nn
                ld      c,R8_NN
                call    exprGet
                ret     c

                ld      a,h
                and     a
                ret     z

                call    errorPrint
                dz      "8-BIT EXPRESSION EXPECTED"
                ret

.not_expr:
                cp      '('
                jr      nz,ldGet8

                inc     e
                push    de

ldGet8:
                inc     e
                ld      c,R8_B
                cp      OP_B
                ret     z

                TEST_OP OP_C
                TEST_OP OP_D
                TEST_OP OP_E
                TEST_OP OP_H
                TEST_OP OP_L

                inc     c
                cp      '('
                jr      z,.paren

                TEST_OP OP_A

                ld      c,R8_IXH
                cp      OP_IXH
                ret     z
                TEST_OP OP_IXL
                TEST_OP OP_IYH
                TEST_OP OP_IYL
                TEST_OP OP_I
                TEST_OP OP_R

.error:
                dec     e
                scf
                ret

.paren:
                push    de
                ld      a,(de)

                ld      c,R8_P_NNNN
                call    exprIsNext
                jr      c,.not_p_expr
                call    exprGet
                jr      c,.paren_error
                jr      .good

.not_p_expr:
                inc     e
                ld      c,R8_HL
                cp      OP_HL
                jr      z,.good

                ld      c,R8_BC
                cp      OP_BC
                jr      z,.good

                inc     c
                cp      OP_DE
                jr      z,.good

                ld      c,R8_IX
                cp      OP_IX
                jr      z,.indexed
                inc     c
                cp      OP_IY
                jr      z,.indexed

.paren_error:
                pop     de
                jr      .error

.good:
                ld      a,(de)
                inc     e
                cp      ')'
                jr      nz,.paren_error

                inc     sp
                inc     sp
                ret

.indexed:
                ld      hl,0
                ld      a,(de)
                cp      '+'
                jr      z,.calc
                cp      '-'
                jr      nz,.good
.calc:
                call    exprGet
                jr      c,.paren_error
                call    asmValidDisp
                jr      c,.paren_error
                jr      .good

;;----------------------------------------------------------------------------------------------------------------------
;; ldGet16(_nn)
;; Convert the token to an index or set CF = 1 if error occurs
;;
;; Input:
;;      A = token
;;      DE = address of token
;;
;; Output:
;;      C = index of 16-bit reference
;;      DE = points after tokens if matched
;;      ZF = 1 if match occurs
;;      DE is unchanged if ZF = 0
;;      HL = address or immediate value
;;

ldGet16_nn:
                ld      c,R16_NNNN
                call    exprIsNext
                jr      c,ldGet16
                call    exprGet
                ret

ldGet16:
                inc     e
                ld      c,R16_BC
                cp      OP_BC
                ret     z

                TEST_OP OP_DE
                TEST_OP OP_HL
                TEST_OP OP_SP
                TEST_OP OP_IX
                TEST_OP OP_IY

                inc     c
                cp      '('
                jr      nz,ldGet8.error

                ld      a,(de)
                push    de
                call    exprIsNext
                jp      c,ldGet8.paren_error
                call    exprGet
                jr      c,ldGet8.paren_error
                jr      ldGet8.good

;;----------------------------------------------------------------------------------------------------------------------
;; 8-bit table
;;
;; Defines the matrix of operations for 8-bit LD opcodes
;;
;; Indexes:
;;      $00 - B
;;      $01 - C
;;      $02 - D
;;      $03 - E
;;      $04 - H
;;      $05 - L
;;      $06 - (HL)
;;      $07 - A
;;      $08 - (BC)
;;      $09 - (DE)
;;      $0A - IXH
;;      $0B - IXL
;;      $0C - IYH
;;      $0D - IYL
;;      $0E - I
;;      $0F - R
;;      $10 - (IX)
;;      $11 - (IY)
;;      $12 - (nnnn)
;;      $13 - nn
;;

LDTable8:       ;       B     C     D     E     H     L     (HL)  A     (BC)  (DE)  IXH   IXL   IYH   IYL   I     R     (IX)  (IY)  (nnnn)nn
                dw      $0040,$0041,$0042,$0043,$0044,$0045,$0046,$0047,$0000,$0000,$44dd,$45dd,$44fd,$45fd,$0000,$0000,$46dd,$46fd,$0000,$0006     ; B
                dw      $0048,$0049,$004a,$004b,$004c,$004d,$004e,$004f,$0000,$0000,$4cdd,$4ddd,$4cfd,$4dfd,$0000,$0000,$4edd,$4efd,$0000,$000e     ; C
                dw      $0050,$0051,$0052,$0053,$0054,$0055,$0056,$0057,$0000,$0000,$54dd,$55dd,$54fd,$55fd,$0000,$0000,$56dd,$56fd,$0000,$0016     ; D
                dw      $0058,$0059,$005a,$005b,$005c,$005d,$005e,$005f,$0000,$0000,$5cdd,$5ddd,$5cfd,$5dfd,$0000,$0000,$5edd,$5efd,$0000,$001e     ; E
                dw      $0060,$0061,$0062,$0063,$0064,$0065,$0066,$0067,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$66dd,$66fd,$0000,$0026     ; H
                dw      $0068,$0069,$006a,$006b,$006c,$006d,$006e,$006f,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$6edd,$6efd,$0000,$002e     ; L
                dw      $0070,$0071,$0072,$0073,$0074,$0075,$0076,$0077,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0036     ; (HL)
                dw      $0078,$0079,$007a,$007b,$007c,$007d,$007e,$007f,$000a,$001a,$7cdd,$7ddd,$7cfd,$7dfd,$57ed,$5fed,$7edd,$7efd,$003a,$003e     ; A
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0002,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000     ; (BC)
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0012,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000     ; (DE)
                dw      $60dd,$61dd,$62dd,$63dd,$0000,$0000,$0000,$67dd,$0000,$0000,$64dd,$65dd,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$26dd     ; IXH
                dw      $68dd,$69dd,$6add,$6bdd,$0000,$0000,$0000,$6fdd,$0000,$0000,$6cdd,$6ddd,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$2edd     ; IXL
                dw      $60fd,$61fd,$62fd,$63fd,$0000,$0000,$0000,$67fd,$0000,$0000,$0000,$0000,$64fd,$65fd,$0000,$0000,$0000,$0000,$0000,$26fd     ; IYH
                dw      $68fd,$69fd,$6afd,$6bfd,$0000,$0000,$0000,$6ffd,$0000,$0000,$0000,$0000,$6cfd,$6dfd,$0000,$0000,$0000,$0000,$0000,$2efd     ; IYL
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$0000,$47ed,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000     ; I
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$0000,$4fed,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000     ; R
                dw      $70dd,$71dd,$72dd,$73dd,$74dd,$75dd,$0000,$77dd,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$36dd     ; (IX)
                dw      $70fd,$71fd,$72fd,$73fd,$74fd,$75fd,$0000,$77fd,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$36fd     ; (IY)
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$0000,$0032,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000,$0000     ; (nnnn)

;;----------------------------------------------------------------------------------------------------------------------
;; 16-bit table
;;
;; Defines the matrix of operations for 16-bit LD opcodes
;;
;;      $00 - BC
;;      $01 - DE
;;      $02 - HL
;;      $03 - SP
;;      $04 - IX
;;      $05 - IY
;;      $06 - (nnnn)
;;      $07 - nnnn
;;

LDTable16       ;       BC    DE    HL    SP    IX    IY    (nnnn)nnnn
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$4bed,$0001         ; BC
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$5bed,$0011         ; DE
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$002a,$0021         ; HL
                dw      $0000,$0000,$00f9,$0000,$f9dd,$f9fd,$7bed,$0031         ; SP
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$2add,$21dd         ; IX
                dw      $0000,$0000,$0000,$0000,$0000,$0000,$2afd,$21fd         ; IY
                dw      $43ed,$53ed,$0022,$73ed,$22dd,$22fd,$0000,$0000         ; (nnnn)

;;----------------------------------------------------------------------------------------------------------------------
;;----------------------------------------------------------------------------------------------------------------------
