#!/usr/bin/env bash
# check for "-h" arg
if [[ "-h" == "$1" ]]; then
    echo "build.sh [<directory1> ...] [<file>.mmc|<file>.img [1] [2]]"
    echo " * if directory(ies) is/are provided, the odin files will be copied there"
    echo " * \".mmc\" or \".img\" file to use hdfmonkey to put odin into image (or set MMC env var)"
    echo " * if MMC image is provided, add 1 or 2 to launch CSpect or ZESERUse with MMC image"
    exit
fi

if [[ "test" == "$1" ]]; then
    # assemble test-runner NEX file
    sjasmplus src/testr.s -D__T_SUITE_1 -I/env/src --zxnext=cspect --msg=war --fullpath --lst --lstlab=sort || exit
    sjasmplus src/testr.s -D__T_SUITE_2 -I/env/src --zxnext=cspect --msg=war --fullpath --lst --lstlab=sort || exit
    runCSpect -brk -map=testr.map testr1.nex
    runCSpect -brk -map=testr.map testr2.nex
    exit
fi

# assemble and merge final binary
sjasmplus src/main.s -I/env/src --zxnext=cspect --msg=war --fullpath && \
cat odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data > odin && \
rm odin_boot odin_shared odin_monitor odin_editor odin_asm odin_dbg odin_data || exit

echo "Odin binary built successfully"

for argx in "$@"; do
    # check if ".img" or ".mmc" file was part of command line, use it as MMC variable then
    if [[ "mmc" == ${argx##*.} || "img" == ${argx##*.} ]]; then
        if [[ -f "$argx" && -s "$argx" && -r "$argx" && -w "$argx" ]]; then
            echo "MMC image filename provided: $argx (odin will be copied to it)"
            MMC="$argx"
        else
            echo "MMC image \"$argx\" not found or not writeable!"
        fi
    fi
    # check if some dir was provided as CLI argument, copy files to it as if it was card
    if [[ -d "$argx" && -r "$argx" && -w "$argx" ]]; then
        echo "Copying odin to provided directory: $argx"
        mkdir -p "$argx/dot" "$argx/docs/guides"
        cp odin "$argx/dot/" && \
        cp docs/odin.gde "$argx/docs/guides/" && \
        cp etc/learn.odn "$argx/" && \
        cp etc/plottest/* "$argx/"
    fi
done

# do the extra copying to provided MMC file or launching when requested
if [[ -f "$MMC" && -s "$MMC" ]]; then
    echo "Copying odin binary to MMC image: $MMC"
    hdfmonkey put "$MMC" odin /dot && \
    hdfmonkey put "$MMC" docs/odin.gde /docs/guides && \
    hdfmonkey put "$MMC" etc/learn.odn / && \
    hdfmonkey put "$MMC" etc/plottest/* /

    # launch emulator if requested ("1" to run CSpect, "2" to run ZESERUse)
    for argx in "$@"; do
        [[ "1" == "$argx" ]] && mmcCSpect "$MMC" -brk   # check etc/mmcCSpect and etc/mmczeseruse
        [[ "2" == "$argx" ]] && mmczeseruse "$MMC"      # for examples of launch script
    done
fi
