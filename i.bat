@echo off
if exist d:\dot (
	echo Copying to SD card...
	copy odin d:\dot
	copy docs\odin.gde d:\docs\guides
)
if exist e:\dot (
    echo Copying to SD card...
    copy odin e:\dot
    copy docs\odin.gde e:\docs\guides
)
if exist f:\dot (
    echo Copying to SD card...
    copy odin f:\dot
    copy docs\odin.gde f:\docs\guides
)

echo Copying to server...
copy odin \sync\server\dot
copy docs\odin.gde \sync\server\docs\guides
