@echo off
call m.bat
if not errorlevel 1 (
    echo Copying autoexec.bas to SD card...
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/autoexec.bas /nextzxos
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/test.asm /
    \env\cspect\CSpect.exe -sound -r -tv -brk -16bit -s28 -w3 -zxnext -nextrom -map=odin.map -mmc=\env\tbblue.mmc
)
