@echo off
\env\sjasmplus\sjasmplus src\main.s -I/env/src --zxnext=cspect --msg=war --fullpath
if not errorlevel 1 (
    echo Generating dot command...
    copy /b odin_boot+odin_shared+odin_monitor+odin_editor+odin_asm+odin_dbg+odin_data odin
    del odin_boot
    del odin_shared
    del odin_monitor
    del odin_editor
    del odin_asm
    del odin_dbg
    del odin_data
    echo Copying Odin to Emulator SD card...
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc odin /dot
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc docs/odin.gde /docs/guides
    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/learn.odn /

    \env\hdfmonkey\hdfmonkey put \env\tbblue.mmc etc/plottest/* /
    call i.bat
)
